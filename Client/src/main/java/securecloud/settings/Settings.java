package securecloud.settings;

/**
 * Default application settings.
 */
public class Settings {

    /**
     * Server port.
     */
    public static final int PORT = 8443;

    /**
     * Service URL.
     */
    public static final String WEB_SERVER_URL = "https://localhost:" + PORT + "/";
}
