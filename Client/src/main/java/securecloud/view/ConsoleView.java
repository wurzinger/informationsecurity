package securecloud.view;

import org.apache.commons.lang3.StringUtils;
import securecloud.controller.interfaces.IShareController;
import securecloud.controller.interfaces.ISyncController;
import securecloud.controller.interfaces.IUserController;
import securecloud.model.entities.User;
import securecloud.model.exceptions.NotInitializedException;
import securecloud.model.exceptions.UserAlreadyLoggedInException;
import securecloud.model.exceptions.UserNotLoggedInException;
import securecloud.view.interfaces.IView;

import java.io.Console;
import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A console view to execute the application functions.
 */
public class ConsoleView implements IView {

    private final String EXIT = "exit";
    private final String EMAIL_REGEX = "^[a-zA-Z0-9_\\.-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-]+$";
    private final String PASSWORD_REGEX = "^(.{1,64}$)";
    private final String NAME_REGEX = "^(.{1,64}$)";
    private final String FILE_PATH_REGEX = "^(.{1,256}$)";

    private IUserController userController = null;
    private ISyncController syncController = null;
    private IShareController shareController = null;

    /**
     * Run the console view.
     * Reads commands from the system input and executes it.
     * Application can be stopped with the 'exit' command.
     */
    public void run() {

        String command;

        try {
            final Scanner scanner = new Scanner(System.in);
            do {
                System.out.print("sc ");
                command = scanner.nextLine().trim();
                if (command.length() > 0 && !EXIT.equalsIgnoreCase(command)) {
                    boolean handled = executeCommand(command);
                    if (!handled)
                        showError("Unknown command '" + command + "'. Try 'help' for more information");
                }
            } while (!EXIT.equalsIgnoreCase(command));
            scanner.close();

            if (syncController.isInitialized()) {
                try {
                    syncController.encryptAll();
                } catch (Exception e) {
                    showError(e.getMessage());
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Set the user controller.
     *
     * @param userController user controller
     */
    public void setUserController(final IUserController userController) {
        this.userController = userController;
    }

    /**
     * Set the synchronisation controller.
     *
     * @param syncController synchronisation controller
     */
    public void setSyncController(final ISyncController syncController) {
        this.syncController = syncController;
    }

    /**
     * Set the sharing controller.
     *
     * @param shareController share controller
     */
    public void setShareController(final IShareController shareController) {
        this.shareController = shareController;
    }

    /**
     * Execute user commands.
     * 'help' command prints all available commands which can be executed.
     *
     * @param input input command with optional parameters
     * @return true if command exists and can be handled else false
     */
    private boolean executeCommand(final String input) {

        boolean handled = true;

        final String[] parameters = input.split("\\s");
        final String command = parameters[0].toLowerCase();
        final String[] values = Arrays.copyOfRange(parameters, 1, parameters.length);

        try {
            switch (command.toLowerCase()) {
                case "register": {
                    if (userController.isUserLoggedIn())
                        throw new UserAlreadyLoggedInException();

                    executeRegistration();
                    break;
                }
                case "login": {
                    if (userController.isUserLoggedIn())
                        throw new UserAlreadyLoggedInException();

                    if (values.length == 0)
                        executeLogin();
                    else if (values.length == 1)
                        executeLogin(values[0]);
                    else if (values.length == 2)
                        executeLogin(values[0], values[1]);
                    break;
                }
                case "logout": {
                    userController.logout();
                    syncController.reset();
                    break;
                }
                case "init": {
                    if (!userController.isUserLoggedIn())
                        throw new UserNotLoggedInException();

                    if (values.length == 0)
                        executeInit();
                    if (values.length == 1)
                        executeInit(values[0]);
                    break;
                }
                case "encrypt": {
                    if (!userController.isUserLoggedIn())
                        throw new UserNotLoggedInException();
                    if (!syncController.isInitialized())
                        throw new NotInitializedException();

                    executeEncrypt();
                    break;
                }
                case "decrypt": {
                    if (!userController.isUserLoggedIn())
                        throw new UserNotLoggedInException();
                    if (!syncController.isInitialized())
                        throw new NotInitializedException();

                    executeDecrypt();
                    break;
                }
                case "share": {
                    if (!userController.isUserLoggedIn())
                        throw new UserNotLoggedInException();
                    if (!syncController.isInitialized())
                        throw new NotInitializedException();

                    if (values.length == 0)
                        executeShare();
                    else if (values.length == 1)
                        executeShare(values[0]);
                    else if (values.length == 2)
                        executeShare(values[0], values[1]);
                    break;
                }
                case "unshare": {
                    if (!userController.isUserLoggedIn())
                        throw new UserNotLoggedInException();
                    if (!syncController.isInitialized())
                        throw new NotInitializedException();

                    if (values.length == 0)
                        executeUnshare();
                    else if (values.length == 1)
                        executeUnshare(values[0]);
                    else if (values.length == 2)
                        executeUnshare(values[0], values[1]);
                    break;
                }
                case "sync": {
                    if (!userController.isUserLoggedIn())
                        throw new UserNotLoggedInException();
                    if (!syncController.isInitialized())
                        throw new NotInitializedException();

                    executeSync();
                    break;
                }
                case "help": {
                    System.out.println("Available commands:");
                    System.out.println("-> encrypt");
                    System.out.println("-> decrypt");
                    System.out.println("-> init [path]");
                    System.out.println("-> login [email] [password]");
                    System.out.println("-> logout ");
                    System.out.println("-> register");
                    System.out.println("-> share [user] [path]");
                    System.out.println("-> sync");
                    break;
                }
                default: {
                    handled = false;
                    break;
                }
            }
        } catch (UserAlreadyLoggedInException e) {
            this.showError("Please logout first if you want to register or login another user");
        } catch (UserNotLoggedInException e) {
            this.showError("Please login first if you want to use this function");
        } catch (NotInitializedException e) {
            this.showError("Please initialize the application first if you want to use this function");
        } catch (CommandAbortedException ex) {
            this.showError("Command aborted");
        } catch (Exception ex) {
            this.showError(ex.toString());
        }

        return handled;
    }

    /**
     * Synchronize local and remote folder.
     */
    private void executeSync() {

        syncController.syncAll();
    }

    /**
     * Share a file with another user.
     * E-mail address of file participant and file to share will be requested through the standard input.
     *
     * @throws CommandAbortedException if exit command has been entered
     */
    private void executeShare()
            throws CommandAbortedException {

        final String email = readValue("User:", EMAIL_REGEX, "Please enter email address of recipient");
        final String path = readValue("File:", FILE_PATH_REGEX, "Please enter file path");
        executeShare(email, path);
    }

    /**
     * Share a file with another user.
     * File to shared will be requested through the standard input.
     *
     * @param email e-mail address of file participant
     * @throws CommandAbortedException if exit command has been entered
     */
    private void executeShare(final String email)
            throws CommandAbortedException {

        final String path = readValue("File:", FILE_PATH_REGEX, "Please enter file path");
        executeShare(email, path);
    }

    /**
     * Share a file with another user.
     *
     * @param email e-mail address of file participant
     * @param path  path to the file which should be shared
     */
    private void executeShare(final String email, final String path) {

        if (shareController.shareFile(email, path))
            showInformation("Sharing file '" + path + "' with user '" + email + "' was successful");
        else
            showError("Sharing file '" + path + "' with user '" + email + "' was unsuccessful");
    }

    /**
     * Revoke access right of a shared file.
     * E-mail address of file participant and shared file will be requested through the standard input.
     *
     * @throws CommandAbortedException if exit command has been entered
     */
    private void executeUnshare()
            throws CommandAbortedException {

        final String email = readValue("User:", EMAIL_REGEX, "Please enter email address of recipient");
        final String path = readValue("File:", FILE_PATH_REGEX, "Please enter file path");
        executeUnshare(email, path);
    }

    /**
     * Revoke access right of a shared file.
     * Shared file will be requested through the standard input.
     *
     * @param email e-mail address of shared file participant
     * @throws CommandAbortedException if exit command has been entered
     */
    private void executeUnshare(final String email) throws CommandAbortedException {

        final String path = readValue("File:", FILE_PATH_REGEX, "Please enter file path");
        executeUnshare(email, path);
    }

    /**
     * Revoke access right of a shared file.
     *
     * @param email e-mail address of shared file participant
     * @param path  path to the shared file
     */
    private void executeUnshare(final String email, final String path) {

        if (shareController.unshareFile(email, path))
            showInformation("Unsharing file '" + path + "' with user '" + email + "' was successful");
        else
            showError("Unsharing file '" + path + "' with user '" + email + "' was unsuccessful");
    }

    /**
     * Decrypt all files on the local disk.
     */
    private void executeDecrypt() {

        try {
            syncController.decryptAll();
        } catch (Exception e) {
            showError(e.getMessage());
        }
    }

    /**
     * Encrypt all files on the local disk.
     */
    private void executeEncrypt() {

        try {
            syncController.encryptAll();
        } catch (Exception e) {
            showError(e.getMessage());
        }
    }

    /**
     * Initialize a new or existing secure directory.
     * Directory path will be requested through the standard input.
     *
     * @throws CommandAbortedException if exit command has been entered
     */
    private void executeInit()
            throws CommandAbortedException {

        final String path = readValue("Directory path:", ".*", "");
        executeInit(path);
    }

    /**
     * Initialize a new or existing secure directory.
     *
     * @param path directory path which should be secured
     */
    private void executeInit(final String path) {

        syncController.initialize(path);
        syncController.decryptAll();
    }

    /**
     * Execute user registration.
     *
     * @throws CommandAbortedException if exit command has been entered
     */
    private void executeRegistration()
            throws CommandAbortedException {

        final User user = new User();
        user.setFirstName(readValue("First name:", NAME_REGEX, "Please enter first name with a length between 1 and 64"));
        user.setLastName(readValue("Last name:", NAME_REGEX, "Please enter last name with a length between 1 and 64"));
        user.setEmail(readValue("Email:", EMAIL_REGEX, "Please enter a valid email address"));
        user.setPassword(readValue("Password:", PASSWORD_REGEX, "Please enter a password with a length between 1 and 64", true));
        readValue("Password match:", user.getPassword(), "Please enter a password with a length between 1 and 64", true);
        userController.register(user);
        showInformation("Registration of user '" + user.getEmail() + "' was successful");
    }

    /**
     * Execute user login.
     * E-mail address and password will be requested through the standard input.
     *
     * @throws CommandAbortedException if exit command has been entered
     */
    private void executeLogin() throws CommandAbortedException {

        final String email = readValue("Email:", EMAIL_REGEX, "Please enter a valid email address");
        final String password = readValue("Password:", PASSWORD_REGEX, "Please enter a password with a length between 1 and 64", true);
        executeLogin(email, password);
    }

    /**
     * Execute user login.
     * User password will be requested through the standard input.
     *
     * @param email e-mail address of user.
     * @throws CommandAbortedException if exit command has been entered
     */
    private void executeLogin(final String email)
            throws CommandAbortedException {

        final String password = readValue("Password:", PASSWORD_REGEX, "Please enter a password with a length between 1 and 64", true);
        executeLogin(email, password);
    }

    /**
     * Execute user login.
     *
     * @param email    e-mail address of user
     * @param password password of user
     */
    private void executeLogin(final String email, final String password) {

        final User user = new User();
        user.setEmail(email);
        user.setPassword(password);
        userController.login(user);
    }


    /**
     * Read input values from the standard input.
     * User input is displayed and not hidden.
     *
     * @param caption      message caption
     * @param regex        input validation regular expression
     * @param errorMessage error message to display if input doesn't match regular expression
     * @return entered input value
     * @throws CommandAbortedException if exit command has been entered
     */
    private String readValue(String caption, final String regex, String errorMessage)
            throws CommandAbortedException {

        return readValue(caption, regex, errorMessage, false);
    }

    /**
     * Read input values from the standard input.
     *
     * @param caption      message caption
     * @param regex        input validation regular expression
     * @param errorMessage error message to display if input doesn't match regular expression
     * @param hiddenInput  true if input should be hidden else false
     * @return entered input value
     * @throws CommandAbortedException if exit command has been entered
     */
    private String readValue(String caption, final String regex, String errorMessage, boolean hiddenInput)
            throws CommandAbortedException {

        if (caption == null) caption = "";
        else caption = caption.trim();
        if (errorMessage == null) errorMessage = "";
        else errorMessage = errorMessage.trim();

        final Pattern pattern = Pattern.compile(regex);
        final Scanner scanner = new Scanner(System.in);
        final Console console = System.console();
        Matcher matcher;
        String value;

        do {

            if (StringUtils.isNotEmpty(caption)) {
                System.out.print(caption + " ");
            }

            // attention: console does not work in IDEs
            if (console != null && hiddenInput) value = new String(console.readPassword());
            else value = scanner.nextLine();

            if (EXIT.equalsIgnoreCase(value)) {
                throw new CommandAbortedException();
            }

            matcher = pattern.matcher(value);
            if (!matcher.matches() && StringUtils.isNotEmpty(errorMessage)) {
                showError(errorMessage);
            }

        } while (!matcher.matches());

        return value;
    }

    /**
     * Display an error message.
     *
     * @param message error message to print
     */
    @Override
    public void showError(final String message) {

        System.err.println(message);
        System.err.flush();

        try {
            // used for correct printing of error and normal messages in the console
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Display an information message.
     *
     * @param message information message to print
     */
    @Override
    public void showInformation(final String message) {

        System.out.println(message);
        System.out.flush();

        try {
            // used for correct printing of error and normal messages in the console
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
