package securecloud.view.interfaces;

/**
 * Necessary view functions which have to be provided.
 */
public interface IView {

    /**
     * Dispaly an error message.
     *
     * @param message error message
     */
    void showError(String message);

    /**
     * Display an information message.
     *
     * @param message information message
     */
    void showInformation(String message);
}
