package securecloud.view;

/**
 * Exception class which signals that an executing command has been aborted.
 */
public class CommandAbortedException extends Exception {

    /**
     * Default constructor.
     */
    public CommandAbortedException() {
        super();
    }
}
