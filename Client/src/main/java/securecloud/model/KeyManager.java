package securecloud.model;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import securecloud.model.entities.MetaData;
import securecloud.model.entities.PublicKey;
import securecloud.model.entities.User;
import securecloud.model.exceptions.InvalidHttpStatusException;
import securecloud.model.exceptions.PublicKeyNotFoundException;
import securecloud.model.exceptions.UserNotLoggedInException;
import securecloud.model.network.Communicator;
import securecloud.model.exceptions.ConnectionException;
import securecloud.model.security.RSA;
import securecloud.settings.Settings;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.security.*;
import java.security.spec.RSAPublicKeySpec;

/**
 * Key manager stores and fetches RSA public keys.
 * Implemented as singleton.
 */
public class KeyManager {

    /**
     * Default constructor.
     */
    private KeyManager() {

    }

    /**
     * Store public key of a user on the remote server.
     *
     * @param publicKey public key to store
     * @throws UserNotLoggedInException   if currently no user is logged in
     * @throws ConnectionException        if connection to the server cannot be established
     * @throws GeneralSecurityException   if RSA specific algorithms cannot be loaded
     * @throws InvalidHttpStatusException if a bad HTTP request occurred
     */
    public void transferPublicKey(final java.security.PublicKey publicKey)
            throws UserNotLoggedInException, ConnectionException,
            GeneralSecurityException, InvalidHttpStatusException {

        final User currentUser = UserManager.getInstance().getCurrentUser();
        if (currentUser == null) {
            throw new UserNotLoggedInException();
        }

        final KeyFactory fact = KeyFactory.getInstance("RSA");
        final RSAPublicKeySpec publicKeySpec = fact.getKeySpec(publicKey, RSAPublicKeySpec.class);
        final PublicKey key = new PublicKey();
        key.setModulus(publicKeySpec.getModulus());
        key.setExponent(publicKeySpec.getPublicExponent());

        final Communicator communicator = new Communicator(currentUser.getEmail(), currentUser.getPassword());

        try {
            communicator.postForEntity(Settings.WEB_SERVER_URL.concat("pkey/store"), key, PublicKey.class);
        } catch (HttpClientErrorException ex) {
            throw new InvalidHttpStatusException(ex);
        }
    }

    /**
     * Fetch public key from a user.
     *
     * @param user user from which the public key should be requested
     * @return RSA public key
     * @throws ConnectionException        if connection to the server cannot be established
     * @throws PublicKeyNotFoundException if user does not exists or hasn't uploaded any public key
     * @throws GeneralSecurityException   if RSA specific algorithms cannot be loaded
     * @throws InvalidHttpStatusException if a bad HTTP request occurred
     */
    public java.security.PublicKey fetchPublicKey(final String user)
            throws ConnectionException, PublicKeyNotFoundException, GeneralSecurityException, InvalidHttpStatusException {

        final Communicator communicator = new Communicator();
        final ResponseEntity<PublicKey> response;

        try {
            response = communicator.postForEntity(Settings.WEB_SERVER_URL.concat("pkey/fetch"), user, PublicKey.class);
        } catch (HttpClientErrorException ex) {
            if (ex.getStatusCode() == HttpStatus.NOT_FOUND)
                throw new PublicKeyNotFoundException(ex);
            else
                throw new InvalidHttpStatusException(ex);
        }

        final KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        final RSAPublicKeySpec publicKeySpec
                = new RSAPublicKeySpec(response.getBody().getModulus(), response.getBody().getExponent());

        return keyFactory.generatePublic(publicKeySpec);
    }

	/**
     *
     * Obtain the decrypted SecretKey from a MetaData file via the logged in user.
     *
     * @param meta a MetaData file containing an encrypted key
     * @return the SecretKey of the MetaData file
     * @throws GeneralSecurityException if the decryption was unsuccessful
     * @throws IOException if the private key could not be read
     */
    public SecretKey decryptKey(final MetaData meta) throws GeneralSecurityException, IOException {
        final User user = UserManager.getInstance().getCurrentUser();
        final KeyPair kp = SyncManager.getInstance().getKeyStorage().loadAsymmetricKeys(user.getPassword());
        final byte[] keyBytes = RSA.decrypt(meta.getKey(), kp.getPrivate());
        return new SecretKeySpec(keyBytes, "AES");
    }

    /**
     * Singleton instance holder.
     */
    private static class SingletonHolder {
        private static final KeyManager INSTANCE = new KeyManager();
    }

    /**
     * Get the key manager singleton instance.
     *
     * @return key manager singleton instance
     */
    public static KeyManager getInstance() {
        return SingletonHolder.INSTANCE;
    }
}