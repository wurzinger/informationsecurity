package securecloud.model.security;

import org.apache.commons.io.FileUtils;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * Utility class that provides some simple and useful static methods for using AES.
 */
public class AES {

	private AES() {
	}

	/**
	 * Simply calls the encryptFile method and writes the bytes back into the same file.
	 *
	 * @param decryptedFile a File that is not encrypted
	 * @param key the key with which to encrypt
	 * @param iv some bytes representing the IV
	 * @throws GeneralSecurityException in case of an encryption error
	 * @throws IOException if the file can't be read
	 */
	public static void encryptAndWriteFile(final File decryptedFile, final SecretKey key, IvParameterSpec iv)
			throws GeneralSecurityException, IOException {
		byte[] encBytes = encryptFile(decryptedFile, key, iv);
		FileUtils.writeByteArrayToFile(decryptedFile, encBytes);
	}

	/**
	 * Reads the given file to into a byte[] and calls the encrypt function, returns the encrypted bytes.
	 *
	 * @param decryptedFile a File that is not encrypted
	 * @param key the key with which to encrypt
	 * @param iv some bytes representing the IV
	 * @return encrypted bytes
	 * @throws GeneralSecurityException in case of an encryption error
	 * @throws IOException if the file can't be read
	 */
	public static byte[] encryptFile(final File decryptedFile, final SecretKey key, IvParameterSpec iv) throws GeneralSecurityException, IOException {
		if (!decryptedFile.exists()) {
			throw new IllegalArgumentException("File does not exist!");
		}

		byte[] decBytes = FileUtils.readFileToByteArray(decryptedFile);

		return AES.encrypt(decBytes, key, iv);
	}

	/**
	 * Basic AES encryption with AES/CBC/PKCS5Padding. Takes data, a key and an IV as input and returns the encrypted
	 * bytes.
	 *
	 * @param data data as byte array
	 * @param key some aes key
	 * @param iv the initialization vector
	 * @return encrypted bytes
	 * @throws GeneralSecurityException in case something wen't wrong during the encryption
	 */
	public static byte[] encrypt(byte[] data, final Key key, IvParameterSpec iv) throws GeneralSecurityException {
		final Cipher encryptCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		encryptCipher.init(Cipher.ENCRYPT_MODE, key, iv);
		return encryptCipher.doFinal(data);
	}

	/**
	 * Calls the decryptFile method and writes the output back into the file.
	 *
	 * @param encryptedFile a File that is encrypted
	 * @param key the key with which it was encrypted
	 * @param iv some bytes representing the IV
	 * @throws GeneralSecurityException in case of an decryption error
	 * @throws IOException if the file can't be read
	 */
	public static void decryptAndWriteFile(final File encryptedFile, final SecretKey key, IvParameterSpec iv) throws GeneralSecurityException, IOException {
		byte[] decBytes = decryptFile(encryptedFile, key, iv);
		FileUtils.writeByteArrayToFile(encryptedFile, decBytes);
	}

	/**
	 * Reads the given file to into a byte[] and calls the decrypt function, returns the decrypted bytes.
	 *
	 * @param encryptedFile a File that is encrypted
	 * @param key the key with which it was encrypted
	 * @param iv some bytes representing the IV
	 * @return the decrypted bytes
	 * @throws GeneralSecurityException in case of an decryption error
	 * @throws IOException if the file can't be read
	 */
	public static byte[] decryptFile(final File encryptedFile, final SecretKey key, IvParameterSpec iv) throws GeneralSecurityException, IOException {
		if (!encryptedFile.exists()) {
			throw new IllegalArgumentException("File does not exist!");
		}

		byte[] encBytes = FileUtils.readFileToByteArray(encryptedFile);

		return AES.decrypt(encBytes, key, iv);
	}

	/**
	 * Basic AES decryption with AES/CBC/PKCS5Padding. Takes data, a key and an IV as input and returns the decrypted
	 * bytes.
	 *
	 * @param data data as byte array
	 * @param key some aes key
	 * @param iv the initialization vector
	 * @return decrypted bytes
	 * @throws GeneralSecurityException in case something wen't wrong during the decryption
	 */
	public static byte[] decrypt(byte[] data, final Key key, IvParameterSpec iv) throws GeneralSecurityException {
		final Cipher decryptCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		decryptCipher.init(Cipher.DECRYPT_MODE, key, iv);
		return decryptCipher.doFinal(data);
	}

	/**
	 * Generates some random 16 byte IV from a SecureRandom.
	 *
	 * @return a random IV
	 */
	public static IvParameterSpec generateIV() {
		final byte[] byteIv = new byte[16];
		final SecureRandom sr = new SecureRandom();
		sr.nextBytes(byteIv);
		return new IvParameterSpec(byteIv);
	}

	/**
	 * Generates a random AES key with 256 bytes.
	 *
	 * @return a random 256 byte AES key
	 * @throws NoSuchAlgorithmException if KeyGenerator stops supporting AES
	 */
	public static SecretKey generateKey() throws NoSuchAlgorithmException {
		final KeyGenerator keyGen = KeyGenerator.getInstance("AES");
		keyGen.init(256);
		return keyGen.generateKey();
	}
}
