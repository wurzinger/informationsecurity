package securecloud.model.security;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

/**
 * Utility class to hash passwords.
 */
public class PBKDF {

    /**
     * Default hashing iterations.
     */
    private static final int ITERATIONS = 1000;

    /**
     * Default hashing key length.
     */
    private static final int KEY_LENGTH = 192;

    /**
     * Default constructor.
     */
    private PBKDF() {

    }

    /**
     * Hash a password with a given salt value.
     * Used hashing algorithm is 'PBKDF2WithHmacSHA1' with 1000 iterations and a key length of 192 bytes.
     *
     * @param password password which should be hashed
     * @param salt     salt value
     * @return hashed password
     */
    public static String hash(final String password, final byte[] salt) {

        try {

            final char[] passwordChars = password.toCharArray();

            final PBEKeySpec spec = new PBEKeySpec(
                    passwordChars,
                    salt,
                    ITERATIONS,
                    KEY_LENGTH
            );

            final SecretKeyFactory key = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
            final byte[] hashedPassword = key.generateSecret(spec).getEncoded();
            return new String(Base64.encodeBase64(hashedPassword));

        } catch (Exception ex) {
            System.err.println(ex.toString());
            return password;
        }
    }
}
