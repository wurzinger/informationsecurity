package securecloud.model.security;

import javax.crypto.Cipher;
import java.security.*;

/**
 * Utility class to generate RSA public and private keys.
 */
public class RSA {

    /**
     * Default constructor.
     */
    private RSA() {

    }

    /**
     * Generate a 4096 bytes long RSA private and public key pair.
     *
     * @return RSA public/private key pair
     * @throws NoSuchAlgorithmException if RSA algorithms can not be loaded
     */
    public static KeyPair generateKeys()
            throws NoSuchAlgorithmException {

        final KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(4096);
        return keyGen.generateKeyPair();
    }

    /**
     * Encrypt data with RSA public key.
     *
     * @param data data to encrypt
     * @param key  RSA public key
     * @return encrypted data
     * @throws GeneralSecurityException if RSA encryption failed
     */
    public static byte[] encrypt(byte[] data, final PublicKey key)
            throws GeneralSecurityException {

        final Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, key);
        return cipher.doFinal(data);
    }

    /**
     * Decrypt data with RSA private key.
     *
     * @param cipherData encrypted data
     * @param key        RSA private key
     * @return decrypted data
     * @throws GeneralSecurityException if RSA decryption failed
     */
    public static byte[] decrypt(byte[] cipherData, final PrivateKey key)
            throws GeneralSecurityException {

        final Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.DECRYPT_MODE, key);
        return cipher.doFinal(cipherData);
    }
}
