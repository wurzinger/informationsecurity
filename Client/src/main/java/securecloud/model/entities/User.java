package securecloud.model.entities;

/**
 * Representation of a user.
 * Used for user registration and user login.
 */
public class User {

    /**
     * First name of user.
     */
    private String firstName;

    /**
     * Last name of user.
     */
    private String lastName;

    /**
     * Hashed user password.
     */
    private String password;

    /**
     * E-mail address of user.
     */
    private String email;

    /**
     * Password salt value.
     */
    private String salt;

    /**
     * Get the first name of a user.
     *
     * @return first name of user
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Set the first name of a user.
     *
     * @param firstName first name of a user
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Get the last name of a user.
     *
     * @return last name of user
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Set the last name of a user.
     *
     * @param lastName last name of a user
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Get the hashed password of a user.
     *
     * @return hashed password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Set the hashed password of a user.
     *
     * @param password hashed password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Get the e-mail address of user.
     *
     * @return e-mail address
     */
    public String getEmail() {
        return email;
    }

    /**
     * Set e-mail address of a user
     *
     * @param email e-mail address
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Get the password salt value.
     *
     * @return salt value
     */
    public String getSalt() {
        return salt;
    }

    /**
     * Set the password salt value.
     *
     * @param salt salt value
     */
    public void setSalt(String salt) {
        this.salt = salt;
    }
}
