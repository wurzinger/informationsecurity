package securecloud.model.entities;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;


public class MetaData implements Serializable {

    /**
     * Relative path of locally stored file.
     */
	private String relativePath;

    /**
     * Encryption and decryption initialization vector.
     */
	private byte[] iv;

    /**
     * Hash value of encrypted file content.
     */
	private byte[] hash;

    /**
     * Flag if file has been encrypted locally.
     */
    private boolean isEncrypted;

    /**
     * Flag if file has been deleted.
     */
	private boolean isDeleted;

    /**
     * Last update time of file content.
     */
	private long lastModified;

    /**
     * E-mail address of file owner.
     */
    private String ownerEMail;

    /**
     * E-mail address of file participant.
     */
    private String participantEMail;

    /**
     * Encrypted key to decrypt file content.
     */
	private byte[] key;

    /**
     * Default constructor.
     */
	public MetaData() {

		relativePath = null;
		iv = null;
		hash = null;
		isEncrypted = false;
		isDeleted = false;
		lastModified = new Date().getTime();
	}

    /**
     * Copy constructor.
     *
     * @param metaData meta data to copy
     */
	public MetaData(MetaData metaData) {

		this.relativePath = metaData.getRelativePath();
		this.iv = metaData.getIv();
		this.hash = metaData.getHash();
		this.isEncrypted = metaData.isEncrypted();
		this.isDeleted = metaData.isDeleted();
		this.lastModified = metaData.getLastModified();
		this.ownerEMail = metaData.getOwnerEMail();
		this.participantEMail = metaData.getParticipantEMail();
		this.key = metaData.getKey();
	}

    /**
     * Check if two objects are equal.
     *
     * @param o object to compare
     * @return true if objects are equal else false
     */
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		MetaData metaData = (MetaData) o;

		if (lastModified != metaData.lastModified) return false;
		if (relativePath != null ? !relativePath.equals(metaData.relativePath) : metaData.relativePath != null)
			return false;
		if (!Arrays.equals(iv, metaData.iv)) return false;
		if (!Arrays.equals(hash, metaData.hash)) return false;
		if (ownerEMail != null ? !ownerEMail.equals(metaData.ownerEMail) : metaData.ownerEMail != null) return false;
		return !(participantEMail != null ? !participantEMail.equals(metaData.participantEMail) : metaData.participantEMail != null);

	}

    /**
     * Generate hash code of the current object.
     *
     * @return hash code
     */
	@Override
	public int hashCode() {
		int result = relativePath != null ? relativePath.hashCode() : 0;
		result = 31 * result + Arrays.hashCode(iv);
		result = 31 * result + Arrays.hashCode(hash);
		result = 31 * result + (int) (lastModified ^ (lastModified >>> 32));
		result = 31 * result + (ownerEMail != null ? ownerEMail.hashCode() : 0);
		result = 31 * result + (participantEMail != null ? participantEMail.hashCode() : 0);
		return result;
	}

    /**
     * Get the encryption/decryption initialization vector.
     *
     * @return initalization vector
     */
	public byte[] getIv() {
		return iv;
	}

    /**
     * Set the encryption/decryption initialization vector.
     *
     * @param iv initialization vector
     */
	public void setIv(byte[] iv) {
		this.iv = iv;
	}

    /**
     * Get encrypted file key.
     *
     * @return encrypted key
     */
	public byte[] getKey() {
		return key;
	}

    /**
     * Set encrypted file key.
     *
     * @param key file key
     */
	public void setKey(byte[] key) {
		this.key = key;
	}

    /**
     * Get hash of encrypted file content.
     *
     * @return hash value
     */
	public byte[] getHash() {
		return hash;
	}

    /**
     * Set hash of encrypted file content.
     *
     * @param hash
     */
	public void setHash(byte[] hash) {
		this.hash = hash;
	}

    /**
     * Get relative path of locally stored file.
     *
     * @return relative file path
     */
	public String getRelativePath() {
		return relativePath;
	}

    /**
     * Set relative path of locally stored file
     *
     * @param relativePath relative file path
     */
	public void setRelativePath(String relativePath) {
		this.relativePath = relativePath;
	}

    /**
     * Get last file update time.
     *
     * @return last update time
     */
	public long getLastModified() {
		return lastModified;
	}

    /**
     * Set the last file update time.
     *
     * @param lastModified new last file update time
     */
	public void setLastModified(long lastModified) {
		this.lastModified = lastModified;
	}

    /**
     * Get e-mail address of file owner.
     *
     * @return e-mail address of file owner
     */
	public String getOwnerEMail() {
		return ownerEMail;
	}

    /**
     * Set e-mail address of file owner.
     *
     * @param ownerEMail e-mail of file owner
     */
	public void setOwnerEMail(String ownerEMail) {
		this.ownerEMail = ownerEMail;
	}

    /**
     * Get e-mail address of shared file participant.
     *
     * @return e-mail address of file participant
     */
	public String getParticipantEMail() {
		return participantEMail;
	}

    /**
     * Set e-mail address of file participant.
     *
     * @param participantEMail e-mail of file participant
     */
	public void setParticipantEMail(String participantEMail) {
		this.participantEMail = participantEMail;
	}

    /**
     * Get flag if file has been encrypted locally.
     *
     * @return true if file has been encrypted else false
     */
	public boolean isEncrypted() {
		return isEncrypted;
	}

    /**
     * Set file encryption status.
     *
     * @param encrypted true if file has been encrypted locally else false
     */
	public void setEncrypted(boolean encrypted) {
		isEncrypted = encrypted;
	}

    /**
     * Get flag if file has been deleted.
     *
     * @return true if file has been deleted else false
     */
	public boolean isDeleted() {
		return isDeleted;
	}

    /**
     * Set file deletion status.
     *
     * @param deleted true if file has been deleted else false
     */
	public void setDeleted(boolean deleted) {
		isDeleted = deleted;
	}
}
