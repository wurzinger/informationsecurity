package securecloud.model.entities;

/**
 * File content wrapper class.
 */
public class Content {

    /**
     * File content.
     */
    private byte[] content;

    /**
     * Default constructor.
     * Used for converting JSON data into Java objects.
     */
    public Content(){
    }

    /**
     * Constructor which sets the file content.
     *
     * @param content file content
     */
    public Content(final byte[] content) {

        this.content = content;
    }

    /**
     * Returns the content of a file.
     *
     * @return content
     */
    public byte[] getContent() {
        return content;
    }
}
