package securecloud.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigInteger;

/**
 * RSA public key representation used to load
 * and transfer public keys from an to the service endpoint.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PublicKey {

    /**
     * Modulus value of the RSA public key.
     */
    private BigInteger modulus;

    /**
     * Exponent value of the RSA public key.
     */
    private BigInteger exponent;

    /**
     * Default constructor.
     */
    public PublicKey() {
    }

    /**
     * Get modulus of RSA public key.
     *
     * @return modulus
     */
    public BigInteger getModulus() {
        return modulus;
    }

    /**
     * Set modulus of RSA public key.
     *
     * @param modulus modulus of RSA public key
     */
    public void setModulus(BigInteger modulus) {
        this.modulus = modulus;
    }

    /**
     * Get exponent of RSA public key.
     *
     * @return exponent
     */
    public BigInteger getExponent() {
        return exponent;
    }

    /**
     * Set exponent of RSA public key.
     *
     * @param exponent exponent of RSA public key
     */
    public void setExponent(BigInteger exponent) {
        this.exponent = exponent;
    }
}
