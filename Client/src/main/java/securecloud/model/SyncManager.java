package securecloud.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import securecloud.model.entities.Data;
import securecloud.model.entities.MetaData;
import securecloud.model.entities.User;
import securecloud.model.exceptions.*;
import securecloud.model.exceptions.FileNotFoundException;
import securecloud.model.network.Communicator;
import securecloud.model.security.AES;
import securecloud.model.security.RSA;
import securecloud.settings.Settings;

import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import java.io.*;
import java.nio.file.Paths;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Synchronisation manager offers function to synchronize local and remote stored files.
 * Implemented as singleton.
 */
public class SyncManager {

    private final String META_DATA_DIR_NAME = ".meta";
    private final String META_DATA_FILE_NAME = ".data";

    /**
     * Directory to operate in
     */
    private File directory;
    /**
     * Path were the meta data is stored
     */
    private String metaDataDirectoryPath;
    /**
     * The actual MetaData file
     */
    private String metaDataFilePath;
    /**
     * Currently loaded MetaData
     */
    private List<MetaData> dirMetaData;
    /**
     * KeyStorage responsible for the RSA keys.
     */
    private KeyStorage keyStorage;

    private SyncManager() {
    }

    /**
     * Initializes the given directory by creating MetaData and all relevant files if they don't exist or loading them
     * if they do.
     *
     * @param directory the folder to initialize in
     * @throws IOException                in the case of an invalid given file
     * @throws GeneralSecurityException   encrypted files could not be decrypted
     * @throws ConnectionException        some connection related error
     * @throws UserNotLoggedInException   if currently no user is logged in
     * @throws DirectoryNotFoundException if base directory does not exist
     * @throws InvalidHttpStatusException if a bad HTTP request occurred
     */
    public void initialize(final File directory)
            throws IOException, GeneralSecurityException, ConnectionException,
            UserNotLoggedInException, InvalidHttpStatusException, DirectoryNotFoundException {

        if (directory == null) {
            throw new IllegalArgumentException("Base directory can not be null");
        } else if (!directory.exists() || !directory.isDirectory()) {
            throw new DirectoryNotFoundException();
        }

        this.directory = directory;
        metaDataDirectoryPath = Paths.get(directory.getPath(), META_DATA_DIR_NAME).toAbsolutePath().toString();
        metaDataFilePath = Paths.get(metaDataDirectoryPath, META_DATA_FILE_NAME).toAbsolutePath().toString();

        final File metaDataDirectory = new File(metaDataDirectoryPath);
        if (!metaDataDirectory.exists()) {
            metaDataDirectory.mkdirs();
        }

        keyStorage = new KeyStorage(metaDataDirectoryPath);

        if (!keyStorage.asymmetricKeysExists()) {
            final KeyPair asymKey = keyStorage.createAsymmetricKeys(UserManager.getInstance().getCurrentUser().getPassword());
            KeyManager.getInstance().transferPublicKey(asymKey.getPublic());
        }

        final File metaDataFile = new File(metaDataFilePath);
        if (!metaDataFile.exists()) {
            dirMetaData = new ArrayList<>();
            storeMetaDataToDisk();
        }

        loadMetaDataFromDisk();
    }

    /**
     * Reset the SyncManager class. Can't be used again until initialized.
     */
    public void reset() {
        directory = null;
        metaDataDirectoryPath = null;
        metaDataFilePath = null;
        dirMetaData = null;
        keyStorage = null;
    }

    public KeyStorage getKeyStorage() {
        return keyStorage;
    }

    public List<MetaData> getDirectoryMetaData() {
        return dirMetaData;
    }

    /**
     * Does as it says, checks from a base directory if any files in subdirectories do not have a
     * MetaData file and create one if not. Works recursively.
     *
     * @param dir directory which is searched.
     */
    public void addUntrackedFiles(final File dir) throws GeneralSecurityException, IOException {
        if (dir == null || !dir.exists()) {
            return;
        }

        MetaData meta = getMetaData(getRelativePath(dir), dirMetaData);
        // if file exists and is not tracked or was deleted and read, add it
        if (dir.isFile() && meta == null) {
            createFileMetaData(dir);
        }

        // don't encrypt my metadata
        if (dir.isDirectory() && !dir.getName().contains(META_DATA_DIR_NAME) && dir.listFiles() != null) {
            for (File f : dir.listFiles()) {
                addUntrackedFiles(f);
            }
        }
    }

    /**
     * Checks if any files were updated, and if so it modifies the corresponding MetaData file.
     *
     * @throws IOException              if the file could not be read
     * @throws NoSuchAlgorithmException if SHA-256 is no longer valid
     */
    public void updateChangedFiles() throws IOException, GeneralSecurityException {
        for (MetaData meta : dirMetaData) {

            File test = new File(directory + File.separator + meta.getRelativePath());

            // if there is no file, just skip it, will be handled by removeDeletedFiles
            if (!test.exists() || test.lastModified() <= meta.getLastModified())
                continue;

            if (meta.isDeleted()) {
                fillFileMetaData(meta);
                continue;
            }

            byte[] data;
            if (!meta.isEncrypted()) {
                data = FileUtils.readFileToByteArray(test);
            } else {
                final SecretKey key = KeyManager.getInstance().decryptKey(meta);
                final IvParameterSpec iv = new IvParameterSpec(meta.getIv());
                data = AES.decryptFile(test, key, iv);
            }

            // either use 256 for performance or 512 for security
            // below insecure, 386 simply uses 512 and cuts of bits
            final MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(data);

            byte[] digest = md.digest();

            if (!MessageDigest.isEqual(digest, meta.getHash())) {
                meta.setHash(digest);
                meta.setLastModified(test.lastModified());
            }
        }
    }

    /**
     * Check if any files were deleted but are still present as MetaData. If so, delete them.
     */
    public void removeDeletedFiles()
            throws KeyStoreException,
            CertificateException, NoSuchAlgorithmException, IOException {

        for (MetaData meta : dirMetaData) {
            final File file = new File(directory + File.separator + meta.getRelativePath());
            if (!file.exists() && !meta.isDeleted()) {
                removeFileMetaData(meta);
            }
        }
    }

    /**
     * Stores the MetaData as a file.
     *
     * @throws IOException if it could not be written
     */
    public void storeMetaDataToDisk() throws IOException {
        try (FileOutputStream fos = new FileOutputStream(metaDataFilePath)) {
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(dirMetaData);
            oos.close();
            fos.close();
        }
    }

    /**
     * Loads the MetaData from a file.
     *
     * @throws IOException if the file could not be read
     */
    private void loadMetaDataFromDisk()
            throws IOException {

        try (FileInputStream fis = new FileInputStream(metaDataFilePath)) {
            ObjectInputStream ois = new ObjectInputStream(fis);
            dirMetaData = (List<MetaData>) ois.readObject();
            ois.close();
            fis.close();
        } catch (ClassNotFoundException e) {
            // should not happen
            e.printStackTrace();
        }
    }

    /**
     * Before the {@see encryptFile(File)} method can be called, the file has to be initialized.
     * <p/>
     * This method generates all necessary data for that, being the key, iv and storing them,
     * as well as the FileMetaData.
     *
     * @param f the file to initialize
     */
    public void createFileMetaData(File f) throws IOException, GeneralSecurityException {
        final MetaData meta = new MetaData();

        // set all data
        meta.setRelativePath(getRelativePath(f));
        fillFileMetaData(meta);

        dirMetaData.add(meta);
    }

    public void fillFileMetaData(MetaData meta) throws GeneralSecurityException, IOException {
        final User user = UserManager.getInstance().getCurrentUser();
        final SecretKey key = AES.generateKey();
        final IvParameterSpec iv = AES.generateIV();
        final KeyPair kp = keyStorage.loadAsymmetricKeys(user.getPassword());

        // encrypt key with user public
        final byte[] encKey = RSA.encrypt(key.getEncoded(), kp.getPublic());

        final File f = Paths.get(getBaseDirectory().getPath(), meta.getRelativePath()).toFile();
        // either use 256 for performance or 512 for security
        // below insecure, 386 simply uses 512 and cuts of bits
        final MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(FileUtils.readFileToByteArray(f));
        byte[] digest = md.digest();

        // set all data
        meta.setLastModified(f.lastModified());
        meta.setHash(digest);
        meta.setIv(iv.getIV());
        meta.setEncrypted(false);
        meta.setDeleted(false);
        meta.setOwnerEMail(user.getEmail());
        meta.setParticipantEMail(user.getEmail());
        meta.setKey(encKey);
    }

    /**
     * Does not actually "delete" the MetaData, necessary to determine when a file was really deleted or someone was just out of sync.
     *
     * @param meta some MetaData
     */
    public void removeFileMetaData(final MetaData meta) {
        // set current time, since it was "changed"
        meta.setLastModified(new Date().getTime());
        meta.setHash(null);
        meta.setDeleted(true);
        meta.setIv(null);
        meta.setEncrypted(false);
        meta.setKey(null);
        meta.setParticipantEMail(null);
    }

    /**
     * Search for a MetaData according to the relative path in a list.
     * Just a simple helper method.
     *
     * @param relativePath some relative path as String
     * @param data         a list of MetaData
     * @return the MetaData with this path or null
     */
    public MetaData getMetaData(String relativePath, List<MetaData> data) {
        for (MetaData entry : data) {
            if (entry.getRelativePath().equals(relativePath)) {
                return entry;
            }
        }
        return null;
    }

    /**
     * Removes a MetaData with the given relative path from the given list.
     * Just a simple helper method.
     *
     * @param relativePath some relative path as String
     * @param data         a list of MetaData
     */
    public void removeMetaData(String relativePath, List<MetaData> data) {
        for (MetaData meta : data) {
            if (meta.getRelativePath().equals(relativePath)) {
                dirMetaData.remove(meta);
                return;
            }
        }
    }

    /**
     * Obtain the relative path between the given file and the base directory.
     * Just a simple helper method.
     *
     * @param subFile some file that has to be in the base directory
     * @return the relative path or null
     */
    public String getRelativePath(final File subFile) {
        return directory.toURI().relativize(subFile.toURI()).getPath();
    }


    public File getBaseDirectory() {
        return directory;
    }

    /**
     * Push the given Data to the server.
     *
     * @param data some Data
     * @return the MetaData of the server
     * @throws ConnectionException        if connection to the server cannot be established
     * @throws UserNotLoggedInException   if no user is currently logged in
     * @throws InvalidHttpStatusException if a bad HTTP request occurred
     * @throws UserNotAuthorizedException if a shared file owner deleted the access to the file
     */
    public MetaData push(final Data data)
            throws InvalidHttpStatusException, UserNotLoggedInException,
            ConnectionException, FileOutOfDateException, UserNotAuthorizedException {

        final User currentUser = UserManager.getInstance().getCurrentUser();
        if (currentUser == null) {
            throw new UserNotLoggedInException();
        }

        final Communicator communicator = new Communicator(currentUser.getEmail(), currentUser.getPassword());
        final ResponseEntity<MetaData> response;

        try {
            response = communicator.postForEntity(Settings.WEB_SERVER_URL.concat("file/store"), data, MetaData.class);
        } catch (HttpClientErrorException ex) {

            if (ex.getStatusCode() == HttpStatus.CONFLICT)
                throw new FileOutOfDateException(data.getMetaData().getRelativePath(), ex);
            else if (ex.getStatusCode() == HttpStatus.UNAUTHORIZED) {
                try {
                    final ObjectMapper mapper = new ObjectMapper();
                    MetaData unauthorized = mapper.readValue(ex.getResponseBodyAsByteArray(), MetaData.class);
                    throw new UserNotAuthorizedException(unauthorized, ex);
                } catch (IOException e) {
                    // should not happen
                    throw new InvalidHttpStatusException(ex);
                }
            } else
                throw new InvalidHttpStatusException(ex);
        }

        return response.getBody();
    }

    /**
     * Pulls the file with the given MetaData from the server.
     *
     * @param data some MetaData file
     * @return file and MetaData stored on the server
     * @throws UserNotLoggedInException   if no user is currently logged in
     * @throws InvalidHttpStatusException if a bad HTTP request occurred
     * @throws FileNotFoundException      if requested file does not exist
     * @throws ConnectionException        a connection related exception
     */
    public Data pull(final MetaData data)
            throws UserNotLoggedInException, InvalidHttpStatusException,
            ConnectionException, FileNotFoundException {

        final User currentUser = UserManager.getInstance().getCurrentUser();
        if (currentUser == null) {
            throw new UserNotLoggedInException();
        }

        final Communicator communicator = new Communicator(currentUser.getEmail(), currentUser.getPassword());
        final ResponseEntity<Data> response;

        try {
            response = communicator.postForEntity(Settings.WEB_SERVER_URL.concat("file"), data, Data.class);
        } catch (HttpClientErrorException ex) {
            if (ex.getStatusCode() == HttpStatus.NOT_FOUND) {
                throw new FileNotFoundException(data.getRelativePath(), ex);
            } else
                throw new InvalidHttpStatusException(ex);
        }

        return response.getBody();
    }

    /**
     * Deletes the file with the given MetaData from the server.
     *
     * @param metaData a MetaData file that is stored on the server
     * @throws UserNotLoggedInException   if no user is currently logged in
     * @throws InvalidHttpStatusException if a bad HTTP request occurred
     * @throws FileOutOfDateException     if a newer file version exists on the server
     * @throws ConnectionException        a connection related exception
     */
    public Long delete(MetaData metaData)
            throws ConnectionException, UserNotLoggedInException, FileOutOfDateException, InvalidHttpStatusException {

        final User currentUser = UserManager.getInstance().getCurrentUser();
        if (currentUser == null) {
            throw new UserNotLoggedInException();
        }

        final Communicator communicator = new Communicator(currentUser.getEmail(), currentUser.getPassword());
        final ResponseEntity<Long> response;

        try {
            response = communicator.postForEntity(Settings.WEB_SERVER_URL.concat("file/delete"), metaData, Long.class);
        } catch (HttpClientErrorException ex) {
            if (ex.getStatusCode() == HttpStatus.CONFLICT)
                throw new FileOutOfDateException(metaData.getRelativePath(), ex);
            else
                throw new InvalidHttpStatusException(ex);
        }

        return response.getBody();
    }

    /**
     * Get all MetaData from the server as array.
     *
     * @return array of MetaData
     * @throws UserNotLoggedInException if no user is currently logged in
     * @throws ConnectionException      a connection related exception
     */
    public MetaData[] update()
            throws ConnectionException, UserNotLoggedInException {

        final User currentUser = UserManager.getInstance().getCurrentUser();
        if (currentUser == null) {
            throw new UserNotLoggedInException();
        }

        final Communicator communicator = new Communicator(currentUser.getEmail(), currentUser.getPassword());
        final ResponseEntity<MetaData[]> response = communicator.exchange(Settings.WEB_SERVER_URL.concat("meta"), HttpMethod.GET, null, MetaData[].class);

        return response.getBody();
    }

    /**
     * Singleton instance holder.
     */
    private static class SingletonHolder {
        private static final SyncManager INSTANCE = new SyncManager();
    }

    /**
     * Get the synchronisation manager singleton instance.
     *
     * @return synchronisation manager singleton instance
     */
    public static SyncManager getInstance() {
        return SingletonHolder.INSTANCE;
    }
}
