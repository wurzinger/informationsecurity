package securecloud.model;

import org.apache.commons.codec.binary.Base64;

import java.security.SecureRandom;
import java.util.Random;

/**
 * Utility class to generate random salt values.
 */
public class SaltGenerator {

    /**
     * Generate a random salt value.
     *
     * @return random salt value
     */
    public static String generate() {

        final Random r = new SecureRandom();
        byte[] salt = new byte[32];
        r.nextBytes(salt);
        return Base64.encodeBase64String(salt);
    }
}
