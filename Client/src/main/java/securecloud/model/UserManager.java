package securecloud.model;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import securecloud.model.entities.User;
import securecloud.model.exceptions.*;
import securecloud.model.network.Communicator;
import securecloud.model.security.PBKDF;
import securecloud.settings.Settings;

/**
 * User manager which offers function to register, login and logout a user.
 * Implemented as singleton.
 */
public class UserManager {

    /**
     * Current logged in user.
     */
    private User currentUser = null;

    /**
     * Communication channel.
     */
    private Communicator communicator;

    /**
     * Default constructor.
     *
     * @throws ConnectionException if no connection to the server can be established
     */
    private UserManager() throws ConnectionException {

        this.communicator = new Communicator();
    }

    /**
     * Register a new user.
     *
     * @param user user which should be registerd
     * @throws UserRegistrationFailedException if user is already registered
     * @throws InvalidHttpStatusException      if a bad HTTP request occurred
     */
    public void register(User user)
            throws UserRegistrationFailedException, InvalidHttpStatusException {

        // transmit only the hashed password
        user.setSalt(SaltGenerator.generate());
        user.setPassword(PBKDF.hash(user.getPassword(), user.getSalt().getBytes()));

        try {
            communicator.postForEntity(Settings.WEB_SERVER_URL.concat("user/register"), user, User.class);
        } catch (HttpClientErrorException ex) {
            if (ex.getStatusCode() == HttpStatus.CONFLICT)
                throw new UserRegistrationFailedException(ex);
            else
                throw new InvalidHttpStatusException(ex);
        }
    }

    /**
     * Authenticate and login a user.
     *
     * @param user user which should be authenticated and logged in
     * @throws UserNotRegisteredException if user is not registered
     * @throws InvalidHttpStatusException if a bad HTTP request occurred
     */
    public void login(User user)
            throws UserNotRegisteredException, InvalidHttpStatusException,
            UserIsLockedException, UserLoginFailedException {

        final ResponseEntity<String> saltResponse;

        try {
            saltResponse = communicator.postForEntity(Settings.WEB_SERVER_URL.concat("user/salt"), user.getEmail(), String.class);
        } catch (HttpClientErrorException ex) {
            if (ex.getStatusCode() == HttpStatus.NOT_FOUND)
                throw new UserNotRegisteredException(ex);
            else
                throw new InvalidHttpStatusException(ex);
        }

        // transmit only the hashed password
        user.setPassword(PBKDF.hash(user.getPassword(), saltResponse.getBody().getBytes()));

        final ResponseEntity<User> userResponse;

        try {
            userResponse = communicator.postForEntity(Settings.WEB_SERVER_URL.concat("user/login"), user, User.class);
        } catch (HttpClientErrorException ex) {
            if (ex.getStatusCode() == HttpStatus.NOT_FOUND)
                throw new UserNotRegisteredException(ex);
            else if (ex.getStatusCode() == HttpStatus.LOCKED)
                throw new UserIsLockedException(ex);
            else if (ex.getStatusCode() == HttpStatus.UNAUTHORIZED)
                throw new UserLoginFailedException(ex);
            else
                throw new InvalidHttpStatusException(ex);
        }

        this.currentUser = userResponse.getBody();
    }

    /**
     * Get the current logged in user.
     *
     * @return current logged in user
     */
    public User getCurrentUser() {

        return currentUser;
    }

    /**
     * Flag if any user is currently logged in.
     *
     * @return true if a user has signed in else false
     */
    public boolean isUserLoggedIn() {
        return currentUser != null;
    }

    /**
     * Logout the currently logged in user.
     */
    public void logout() {

        this.currentUser = null;
    }

    /**
     * Singleton instance holder.
     */
    private static class SingletonHolder {
        private static final UserManager INSTANCE;

        static {
            try {
                INSTANCE = new UserManager();
            } catch (Exception e) {
                throw new ExceptionInInitializerError(e);
            }
        }
    }

    /**
     * Get the user manager singleton instance.
     *
     * @return user manager singleton instance
     */
    public static UserManager getInstance() {
        return SingletonHolder.INSTANCE;
    }
}
