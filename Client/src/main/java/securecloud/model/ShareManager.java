package securecloud.model;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import securecloud.model.entities.MetaData;
import securecloud.model.entities.User;
import securecloud.model.exceptions.*;
import securecloud.model.network.Communicator;
import securecloud.settings.Settings;

/**
 * Share manager offers function to share and unshare a file with multiple users.
 * Implemented as singleton.
 */
public class ShareManager {

    /**
     * Default constructor.
     */
    private ShareManager() {

    }

    /**
     * Share a file with another user.
     *
     * @param metaData meta data of the file which should be shared
     * @throws ConnectionException        if connection to the server cannot be established
     * @throws UserNotLoggedInException   if no user is currently logged in
     * @throws InvalidHttpStatusException if a bad HTTP request occurred
     * @throws FileAlreadyExistsException if the participant already has access to the shared file or owns a file with the same name
     */
    public void share(final MetaData metaData)
            throws ConnectionException, UserNotLoggedInException,
            InvalidHttpStatusException, FileAlreadyExistsException,
            FileNotFoundException {

        final User currentUser = UserManager.getInstance().getCurrentUser();
        if (currentUser == null) {
            throw new UserNotLoggedInException();
        }

        final Communicator communicator = new Communicator(currentUser.getEmail(), currentUser.getPassword());
        try {
            communicator.postForEntity(
                    Settings.WEB_SERVER_URL.concat("share"), metaData, MetaData.class);
        } catch (HttpClientErrorException ex) {
            if (ex.getStatusCode() == HttpStatus.FORBIDDEN)
                throw new FileAlreadyExistsException(ex);
            if (ex.getStatusCode() == HttpStatus.NOT_FOUND)
                throw new FileNotFoundException(ex);
            else
                throw new InvalidHttpStatusException(ex);
        }
    }

    /**
     * Revoke access right of a shared file.
     *
     * @param metaData meta data of file which should be revoked from a participant
     * @throws ConnectionException        if connection to the server cannot be established
     * @throws UserNotLoggedInException   if no user is currently logged in
     * @throws FileOutOfDateException     if a newer file version exists on the server
     * @throws InvalidHttpStatusException if a bad HTTP request occurred
     */
    public void unshare(final MetaData metaData)
            throws ConnectionException, UserNotLoggedInException, InvalidHttpStatusException, FileOutOfDateException, UserNotAuthorizedException {

        final User currentUser = UserManager.getInstance().getCurrentUser();
        if (currentUser == null) {
            throw new UserNotLoggedInException();
        }

        try {
            final Communicator communicator = new Communicator(currentUser.getEmail(), currentUser.getPassword());
            communicator.postForEntity(
                    Settings.WEB_SERVER_URL.concat("unshare"), metaData, MetaData.class);
        } catch (HttpClientErrorException ex) {
            if (ex.getStatusCode() == HttpStatus.CONFLICT)
                throw new FileOutOfDateException(metaData.getRelativePath(), ex);
            if (ex.getStatusCode() == HttpStatus.UNAUTHORIZED)
                throw new UserNotAuthorizedException(ex);
            else
                throw new InvalidHttpStatusException(ex);
        }
    }

    /**
     * Singleton instance holder.
     */
    private static class SingletonHolder {
        private static final ShareManager INSTANCE = new ShareManager();
    }

    /**
     * Get the share manager singleton instance.
     *
     * @return share manager singleton instance
     */
    public static ShareManager getInstance() {
        return SingletonHolder.INSTANCE;
    }
}
