package securecloud.model;

import org.apache.commons.io.FileUtils;
import securecloud.model.security.AES;
import securecloud.model.security.RSA;

import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.math.BigInteger;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;

/**
 * Key Storage stores and loads RSA private and public key.
 */
public class KeyStorage {

    /**
     * Private key file path.
     */
    private final String PRIVATE_KEY_PATH;

    /**
     * Public key file path.
     */
    private final String PUBLIC_KEY_PATH;

    /**
     * Salt value, used for password hashing.
     */
    private final String SALT;

    /**
     * Encryption initialization vector.
     */
    private final byte[] IV;

    /**
     * Default constructor.
     * Initialize key storage path.
     *
     * @param path load/store directory of asymmetric keys
     * @throws FileNotFoundException if directory does not exist
     */
    public KeyStorage(final String path)
            throws FileNotFoundException {

        final File directory = new File(path);
        if (!directory.exists() || !directory.isDirectory()) {
            throw new FileNotFoundException("Directory '" + path + "' does not exist");
        }

        this.PRIVATE_KEY_PATH = Paths.get(directory.getAbsolutePath(), ".private-key").toString();
        this.PUBLIC_KEY_PATH = Paths.get(directory.getAbsolutePath(), ".public-key").toString();
        this.SALT = "a+9erxX3!S?*1";
        this.IV = new byte[]{
                (byte) 0xc7, (byte) 0x73, (byte) 0x21, (byte) 0x8c,
                (byte) 0x7e, (byte) 0xc8, (byte) 0xee, (byte) 0x99,
                (byte) 0x6b, (byte) 0xe8, (byte) 0xaa, (byte) 0x23,
                (byte) 0xdd, (byte) 0xbb, (byte) 0x09, (byte) 0x13
        };
    }

    /**
     * Create a new private and public RSA key.
     * Private key will be stored encrypted on the disk.
     *
     * @param password encryption password
     * @return RSA public/private key pair
     * @throws GeneralSecurityException if RSA algorithms can not be loaded
     * @throws IOException              if private/public key already exists or if IO errors occur
     */
    public KeyPair createAsymmetricKeys(final String password)
            throws GeneralSecurityException, IOException {

        if (asymmetricKeysExists()) {
            throw new FileAlreadyExistsException("Public/Private key already exists");
        }

        final KeyPair keyPair = RSA.generateKeys();
        final KeyFactory fact = KeyFactory.getInstance("RSA");
        final RSAPublicKeySpec publicKey = fact.getKeySpec(keyPair.getPublic(), RSAPublicKeySpec.class);
        final RSAPrivateKeySpec privateKey = fact.getKeySpec(keyPair.getPrivate(), RSAPrivateKeySpec.class);

        try (final ByteArrayOutputStream byteStream = new ByteArrayOutputStream()) {
            try (final ObjectOutputStream objectOutput = new ObjectOutputStream(byteStream)) {
                objectOutput.writeObject(privateKey.getModulus());
                objectOutput.writeObject(privateKey.getPrivateExponent());
                objectOutput.flush();
            }

            byteStream.flush();

            byte[] encryptedData = AES.encrypt(byteStream.toByteArray(), getSecretKey(password), new IvParameterSpec(IV));
            try (final FileOutputStream outputStream = new FileOutputStream(PRIVATE_KEY_PATH)) {
                outputStream.write(encryptedData);
                outputStream.flush();
            }
        }

        try (final ObjectOutputStream objectOutput = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(PUBLIC_KEY_PATH)))) {
            objectOutput.writeObject(publicKey.getModulus());
            objectOutput.writeObject(publicKey.getPublicExponent());
            objectOutput.flush();
        }

        return keyPair;
    }

    /**
     * Get AES key created from SHA-256 hashed password salted with a predefined value.
     *
     * @param password encryption password
     * @return AES key
     * @throws UnsupportedEncodingException if UTF-8 encoding is not supported
     * @throws NoSuchAlgorithmException     if SHA-256 or AES algorithms are not available
     */
    private SecretKeySpec getSecretKey(final String password)
            throws UnsupportedEncodingException, NoSuchAlgorithmException {

        byte[] key = (SALT + password).getBytes("UTF-8");
        final MessageDigest sha = MessageDigest.getInstance("SHA-256");
        key = sha.digest(key);
        return new SecretKeySpec(key, "AES");
    }

    /**
     * Load assymetric keys from disk by providing a decryption password.
     *
     * @param password decryption password
     * @return RSA public/private key pair
     * @throws GeneralSecurityException if RSA algorithms can not be loaded
     * @throws IOException              if private/public key already exists or if IO errors occur
     */
    public KeyPair loadAsymmetricKeys(final String password) throws GeneralSecurityException, IOException {
        if (!asymmetricKeysExists()) {
            throw new FileNotFoundException("Public/Private key does not exist");
        }

        BigInteger publicModulus = null;
        BigInteger publicExponent = null;
        BigInteger privateModulus = null;
        BigInteger privateExponent = null;

        try (final ObjectInputStream input = new ObjectInputStream(new FileInputStream(PUBLIC_KEY_PATH))) {
            publicModulus = (BigInteger) input.readObject();
            publicExponent = (BigInteger) input.readObject();
        } catch (ClassNotFoundException e) {
            // should not happen anyway
            e.printStackTrace();
        }

        byte[] encryptedPrivateKey = FileUtils.readFileToByteArray(new File(PRIVATE_KEY_PATH));
        byte[] decryptedPrivateKey = AES.decrypt(encryptedPrivateKey, getSecretKey(password), new IvParameterSpec(IV));

        try (final ObjectInputStream input = new ObjectInputStream(new ByteArrayInputStream(decryptedPrivateKey))) {
            privateModulus = (BigInteger) input.readObject();
            privateExponent = (BigInteger) input.readObject();
        } catch (ClassNotFoundException e) {
            // should not happen anyway
            e.printStackTrace();
        }

        final KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        final RSAPublicKeySpec publicKeySpec = new RSAPublicKeySpec(publicModulus, publicExponent);
        final PublicKey publicKey = keyFactory.generatePublic(publicKeySpec);
        final RSAPrivateKeySpec privateKeySpec = new RSAPrivateKeySpec(privateModulus, privateExponent);
        final PrivateKey privateKey = keyFactory.generatePrivate(privateKeySpec);

        return new KeyPair(publicKey, privateKey);
    }

    /**
     * Flag if asymmetric keys exist in the initialized key storage directory.
     *
     * @return true if private and public key file exist on disk
     */
    public boolean asymmetricKeysExists() {

        return Files.exists(Paths.get(PRIVATE_KEY_PATH)) && Files.exists(Paths.get(PUBLIC_KEY_PATH));
    }
}
