package securecloud.model.exceptions;

/**
 * This exception will be thrown an object is not initialized.
 */
public class NotInitializedException extends Exception {

    /**
     * Default constructor.
     */
    public NotInitializedException() {

    }

    /**
     * Exception wrapper constructor.
     *
     * @param ex source exception
     */
    public NotInitializedException(Exception ex) {
        super(ex);
    }
}
