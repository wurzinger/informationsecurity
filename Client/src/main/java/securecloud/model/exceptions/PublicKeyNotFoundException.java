package securecloud.model.exceptions;

/**
 * This exception will be thrown if public key of a user doesn't exist.
 */
public class PublicKeyNotFoundException extends Exception {

    /**
     * Default constructor.
     */
    public PublicKeyNotFoundException() {

    }

    /**
     * Exception wrapper constructor.
     *
     * @param ex source exception
     */
    public PublicKeyNotFoundException(Exception ex) {

        super(ex);
    }
}