package securecloud.model.exceptions;

/**
 * This exception will be thrown if a specified directory does not exist.
 */
public class DirectoryNotFoundException extends Exception {

    /**
     * Default constructor.
     */
    public DirectoryNotFoundException() {

    }

    /**
     * Exception wrapper constructor.
     *
     * @param ex source exception
     */
    public DirectoryNotFoundException(Exception ex) {

        super(ex);
    }
}