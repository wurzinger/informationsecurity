package securecloud.model.exceptions;

/**
 * This exception will be thrown if an unknown Http state have been recognized.
 */
public class InvalidHttpStatusException extends Exception {

    /**
     * Default constructor.
     */
    public InvalidHttpStatusException() {

    }

    /**
     * Exception wrapper constructor.
     *
     * @param ex source exception
     */
    public InvalidHttpStatusException(Exception ex) {

        super(ex);
    }
}