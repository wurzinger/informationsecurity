package securecloud.model.exceptions;

/**
 * This exception will be thrown if a user registration failed,
 * e.g. a user with the same e-mail address already exists.
 */
public class UserRegistrationFailedException extends Exception {

    /**
     * Default constructor.
     */
    public UserRegistrationFailedException() {

    }

    /**
     * Exception wrapper constructor.
     *
     * @param ex source exception
     */
    public UserRegistrationFailedException(Exception ex) {

        super(ex);
    }
}