package securecloud.model.exceptions;

/**
 * This exception will be thrown if a requested file does not exist.
 */
public class FileNotFoundException extends Exception {

    private final String filePath;

    /**
     * Default constructor.
     */
    public FileNotFoundException() {
        this.filePath = "";
    }

    /**
     * Exception wrapper constructor.
     *
     * @param ex source exception
     */
    public FileNotFoundException(Exception ex) {

        super(ex);
        this.filePath = "";
    }

    /**
     * Exception wrapper constructor.
     *
     * @param ex source exception
     */
    public FileNotFoundException(String filePath, Exception ex) {

        super(ex);
        this.filePath = filePath;
    }

    /**
     * Get the missing file path.
     *
     * @return missing file path
     */
    public String getFilePath() {

        return this.filePath;
    }
}