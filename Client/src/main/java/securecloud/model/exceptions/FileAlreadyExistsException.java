package securecloud.model.exceptions;

/**
 * This exception will be thrown if a user already owns/participates a file
 * with the same name like the new to be shared file.
 */
public class FileAlreadyExistsException extends Exception {

    /**
     * Default constructor.
     */
    public FileAlreadyExistsException() {

    }

    /**
     * Exception wrapper constructor.
     *
     * @param ex source exception
     */
    public FileAlreadyExistsException(Exception ex) {

        super(ex);
    }
}