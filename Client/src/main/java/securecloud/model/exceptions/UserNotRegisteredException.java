package securecloud.model.exceptions;

/**
 * This exception will be thrown if a user cannot be found.
 */
public class UserNotRegisteredException extends Exception {

    /**
     * Default constructor.
     */
    public UserNotRegisteredException() {

    }

    /**
     * Exception wrapper constructor.
     *
     * @param ex source exception
     */
    public UserNotRegisteredException(Exception ex) {

        super(ex);
    }
}