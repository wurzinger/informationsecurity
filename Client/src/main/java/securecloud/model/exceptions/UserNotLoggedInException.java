package securecloud.model.exceptions;

/**
 * This exception will be thrown if a user wants to access protected services
 * without any authentication.
 */
public class UserNotLoggedInException extends Exception {

    /**
     * Default constructor.
     */
    public UserNotLoggedInException() {

    }

    /**
     * Exception wrapper constructor.
     *
     * @param ex source exception
     */
    public UserNotLoggedInException(Exception ex) {

        super(ex);
    }
}