package securecloud.model.exceptions;

/**
 * This exception will be thrown if user authentication failed.
 */
public class UserLoginFailedException extends Exception {

    /**
     * Default constructor.
     */
    public UserLoginFailedException() {

    }

    /**
     * Exception wrapper constructor.
     *
     * @param ex source exception
     */
    public UserLoginFailedException(Exception ex) {

        super(ex);
    }
}