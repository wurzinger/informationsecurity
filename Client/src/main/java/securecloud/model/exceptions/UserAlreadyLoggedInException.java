package securecloud.model.exceptions;

/**
 * This exception will be thrown if a user is already logged in and wants to sign in again.
 */
public class UserAlreadyLoggedInException extends Exception {

    /**
     * Default constructor.
     */
    public UserAlreadyLoggedInException() {

    }

    /**
     * Exception wrapper constructor.
     *
     * @param ex source exception
     */
    public UserAlreadyLoggedInException(Exception ex) {

        super(ex);
    }
}