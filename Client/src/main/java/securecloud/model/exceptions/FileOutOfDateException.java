package securecloud.model.exceptions;

/**
 * This exception will be thrown if a user wants to update a file
 * for which a newer version exists.
 */
public class FileOutOfDateException extends Exception {

    private final String filePath;

    /**
     * Default constructor.
     */
    public FileOutOfDateException() {
        this.filePath = "";
    }

    /**
     * Exception wrapper constructor.
     *
     * @param ex source exception
     */
    public FileOutOfDateException(Exception ex) {

        super(ex);
        this.filePath = "";
    }

    /**
     * Exception wrapper constructor.
     *
     * @param ex source exception
     */
    public FileOutOfDateException(String filePath, Exception ex) {

        super(ex);
        this.filePath = filePath;
    }

    /**
     * Get the missing file path.
     *
     * @return missing file path
     */
    public String getFilePath() {

        return this.filePath;
    }
}