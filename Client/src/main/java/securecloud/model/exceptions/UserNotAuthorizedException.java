package securecloud.model.exceptions;

import securecloud.model.entities.MetaData;

/**
 * This exception will be thrown if a user wants to execute an action
 * without any permission.
 */
public class UserNotAuthorizedException extends Exception {

    private final MetaData meta;

    /**
     * Default constructor.
     */
    public UserNotAuthorizedException() {
        this.meta = null;
    }

    /**
     * Exception wrapper constructor.
     *
     * @param ex source exception
     */
    public UserNotAuthorizedException(Exception ex) {

        super(ex);
        this.meta = null;
    }

    /**
     * Exception wrapper constructor with unauthorized meta data.
     *
     * @param ex source exception
     */
    public UserNotAuthorizedException(MetaData meta, Exception ex) {

        super(ex);
        this.meta = meta;
    }

    public MetaData getMeta() {
        return meta;
    }
}