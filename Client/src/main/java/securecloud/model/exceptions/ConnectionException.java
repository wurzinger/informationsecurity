package securecloud.model.exceptions;

/**
 * This exception will be thrown if a connection to the service endpoint failed.
 */
public class ConnectionException extends Exception {

    /**
     * Default constructor.
     */
    public ConnectionException() {

    }

    /**
     * Exception wrapper constructor.
     *
     * @param ex source exception
     */
    public ConnectionException(Exception ex) {
        super(ex);
    }
}
