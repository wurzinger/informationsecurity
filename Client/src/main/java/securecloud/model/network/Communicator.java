package securecloud.model.network;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.ssl.SSLContexts;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import securecloud.model.exceptions.ConnectionException;

import javax.net.ssl.SSLContext;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

/**
 * Object which enables communication with the service endpoint.
 * Extends the RestTemplate from the Spring framework and provides
 * additionally basic authentication functions.
 * SSL connections are used to access services from the service endpoint.
 */
public class Communicator extends RestTemplate {

    /**
     * Default constructor.
     * Creates a SSL communication channel with no basic authentication.
     *
     * @throws ConnectionException if no connection to the server can be established
     */
    public Communicator()
            throws ConnectionException {

        this.init(null, null);
    }

    /**
     * Creates a SSL communication channel with basic authentication.
     *
     * @param user     user name for basic authentication
     * @param password hashed password for basic authentication
     * @throws ConnectionException if no connection to the server can be established
     */
    public Communicator(final String user, final String password) throws ConnectionException {

        if (user == null || StringUtils.isEmpty(user.trim())) {
            throw new IllegalArgumentException("User name is null or empty");
        }

        if (password == null || StringUtils.isEmpty(password.trim())) {
            throw new IllegalArgumentException("Password hash is null or empty");
        }

        this.init(user, password);
    }

    /**
     * Initializes a SSL communication channel with or without basic authentication.
     *
     * @param user     user name for basic authentication, if null no basic authentication will be used
     * @param password hashed password for basic authentication, if null no basic authentication will be used
     * @throws ConnectionException if no connection to the server can be established
     */
    private void init(final String user, final String password) throws ConnectionException {

        try {

            final SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(null, new TrustSelfSignedStrategy()).useProtocol("TLS").build();
            // Attention: SSL certificate not validated with NoopHostnameVerifier, used because of self signed certificate
            final SSLConnectionSocketFactory connectionFactory = new SSLConnectionSocketFactory(sslContext, new NoopHostnameVerifier());
            final HttpClientBuilder clientBuilder = HttpClientBuilder.create().setSSLSocketFactory(connectionFactory);
            if (user != null && password != null) {
                final BasicCredentialsProvider credentialsProvider = new BasicCredentialsProvider();
                credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(user, password));
                clientBuilder.setDefaultCredentialsProvider(credentialsProvider);
            }
            final HttpClient httpClient = clientBuilder.build();
            this.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient));

        } catch (KeyStoreException | NoSuchAlgorithmException | KeyManagementException ex) {
            throw new ConnectionException(ex);
        }
    }
}
