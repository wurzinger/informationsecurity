package securecloud.controller;

import org.apache.commons.io.FileUtils;
import securecloud.controller.interfaces.ISyncController;
import securecloud.model.KeyManager;
import securecloud.model.SyncManager;
import securecloud.model.UserManager;
import securecloud.model.entities.Content;
import securecloud.model.entities.Data;
import securecloud.model.entities.MetaData;
import securecloud.model.exceptions.*;
import securecloud.model.security.AES;
import securecloud.view.interfaces.IView;

import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

/**
 * Data syncing has two major parts, the local and the remote sync.
 * <p/>
 * <p/>
 * First of all, sync the local metadata with the actual state of the files.
 * Then obtain the remote state of the metadata.
 * <p/>
 * Local metadata that is not in the server has to be added remotely (push), remote metadata that is not
 * tracked locally needs to get added locally (pull).
 * <p/>
 * Local metadata that is newer and not deleted needs to be update on the server (push), newer remote data needs to be
 * updated locally (pull).
 * <p/>
 * If the local meta data was removed AND is newer, the server side needs to remove it to (somewhat push), in the
 * reverse case, the local file needs to be deleted.
 */
public class SyncController implements ISyncController {

    private final SyncManager syncManager;
    private final IView view;
    // lists that will be filled by the checkAll method
    private final List<MetaData> toPush;
    private final List<MetaData> toPull;
    private final List<MetaData> toRemoveLocal;
    private final List<MetaData> toRemoveRemote;

    private int toPushCount;
    private int toPullCount;
    private int toRemoveLocalCount;
    private int toRemoveRemoteCount;
    private boolean isInitialized = false;

    /**
     * Constructor, needs a view for printing error messages.
     *
     * @param view some view
     */
    public SyncController(IView view) {
        if (view == null)
            throw new IllegalArgumentException("View can't be null.");

        this.view = view;
        this.syncManager = SyncManager.getInstance();
        this.toPush = new ArrayList<>();
        this.toPull = new ArrayList<>();
        this.toRemoveLocal = new ArrayList<>();
        this.toRemoveRemote = new ArrayList<>();
    }

    /**
     * Initializes the SyncManager.
     */
    @Override
    public void initialize(final String basePath) {

        try {

            if (!UserManager.getInstance().isUserLoggedIn())
                throw new UserNotLoggedInException();

            if (isInitialized())
                reset();

            final File baseDir = new File(basePath);

            syncManager.initialize(baseDir);
            syncLocal();
            syncManager.storeMetaDataToDisk();

            isInitialized = true;

        } catch (UserNotLoggedInException ex) {
            view.showError("Currently no user is logged in, please sign in");
        } catch (ConnectionException ex) {
            view.showError("Server communication error");
        } catch (DirectoryNotFoundException ex) {
            view.showError("Base directory does not exist");
        } catch (Exception ex) {
            view.showError("Unknown error occurred: " + ex.toString());
        }
    }

    /**
     * Resets SyncManager and clears lists.
     */
    @Override
    public void reset() {

        syncManager.reset();
        toPush.clear();
        toPull.clear();
        toRemoveLocal.clear();
        toRemoveRemote.clear();

        isInitialized = false;
    }

    /**
     * Encrypts all tracked files
     */
    @Override
    public void encryptAll() {
        try {
            if (!UserManager.getInstance().isUserLoggedIn())
                throw new UserNotLoggedInException();

            final List<MetaData> dirMetaData = syncManager.getDirectoryMetaData();

            int i = 0;
            for (final MetaData meta : dirMetaData) {

                if (meta.isEncrypted()) {
                    // does not matter if its already encrypted, nothing to do
                    continue;
                }

                final File path = Paths.get(syncManager.getBaseDirectory().getAbsolutePath(), meta.getRelativePath()).toFile();
                if (path.exists()) {
                    try {
                        // get key
                        final long modify = path.lastModified();

                        final SecretKey key = KeyManager.getInstance().decryptKey(meta);
                        final IvParameterSpec iv = new IvParameterSpec(meta.getIv());
                        AES.encryptAndWriteFile(path, key, iv);
                        meta.setEncrypted(true);
                        path.setLastModified(modify);

                        i++;
                    } catch (GeneralSecurityException | IOException ex) {
                        view.showError("Failed to encrypt " + meta.getRelativePath() + ", reason:\n" + ex.getMessage());
                    }
                }
            }

            syncManager.storeMetaDataToDisk();

            view.showInformation(i + " files encrypted");

        } catch (UserNotLoggedInException ex) {
            view.showError("Currently no user is logged in, please sign in");
        } catch (Exception ex) {
            view.showError("Unknown error occurred: " + ex.toString());
        }
    }

    /**
     * Decrypts all tracked files
     */
    @Override
    public void decryptAll() {

        try {
            if (!UserManager.getInstance().isUserLoggedIn())
                throw new UserNotLoggedInException();

            final List<MetaData> dirMetaData = syncManager.getDirectoryMetaData();

            int i = 0;
            for (MetaData meta : dirMetaData) {

                if (!meta.isEncrypted()) {
                    // whatever, just ignore it if it is already decrypted
                    continue;
                }

                final File path = Paths.get(syncManager.getBaseDirectory().toString(), meta.getRelativePath()).toFile();
                if (path.exists()) {
                    try {
                        // get key
                        final long modify = path.lastModified();

                        final SecretKey key = KeyManager.getInstance().decryptKey(meta);
                        final IvParameterSpec iv = new IvParameterSpec(meta.getIv());
                        AES.decryptAndWriteFile(path, key, iv);
                        meta.setEncrypted(false);
                        // encrypting should not change the last modify date
                        path.setLastModified(modify);

                        i++;
                    } catch (GeneralSecurityException | IOException ex) {
                        view.showError("Failed to decrypt " + meta.getRelativePath() + ", reason:\n" + ex.getMessage());
                    }
                }
            }

            syncManager.storeMetaDataToDisk();

            view.showInformation(i + " files decrypted");

        } catch (UserNotLoggedInException ex) {
            view.showError("Currently no user is logged in, please sign in");
        } catch (Exception ex) {
            view.showError("Unknown error occurred: " + ex.toString());
        }
    }

    /**
     * This method will push a certain file, specified in MetaData, to the server.
     * If the file is not already encrypted, it will be before sending it.
     *
     * @param meta a MetaData file pointing to some File
     */
    private void pushFile(final MetaData meta)
            throws IOException, GeneralSecurityException, UserNotLoggedInException,
            InvalidHttpStatusException, ConnectionException, UserNotAuthorizedException, FileOutOfDateException {

        try {
            final Data data = new Data();
            final File file = Paths.get(syncManager.getBaseDirectory().toString(), meta.getRelativePath()).toFile();

            // if the file is already encrypted, just add it, else encrypt it beforehand
            if (meta.isEncrypted()) {
                data.setContent(new Content(FileUtils.readFileToByteArray(file)));
            } else {

                final SecretKey key = KeyManager.getInstance().decryptKey(meta);
                final IvParameterSpec iv = new IvParameterSpec(meta.getIv());
                final byte[] enc = AES.encryptFile(file, key, iv);

                data.setContent(new Content(enc));
            }

            data.setMetaData(meta);
            final MetaData result = syncManager.push(data);
            meta.setLastModified(result.getLastModified());

        } catch (UserNotAuthorizedException ex) {
            view.showError("File '" + meta.getRelativePath() + "' will be deleted instead of pushed, because owner removed your file access");
            toRemoveLocalCount++;
            toPushCount--;
            removeLocal(ex.getMeta());
        }
    }

    /**
     * Deletes the file that MetaData points to from the server.
     *
     * @param meta some MetaData file
     */
    private void removeRemote(final MetaData meta)
            throws ConnectionException, UserNotLoggedInException,
            FileOutOfDateException, InvalidHttpStatusException {

        final long updateTime = syncManager.delete(meta);
        meta.setLastModified(updateTime);
    }

    /**
     * Removes a local file the given MetaData points to, and replaces the stored MetaData.
     *
     * @param meta some MetaData file
     */
    private void removeLocal(final MetaData meta) {

        final List<MetaData> dirMetaData = syncManager.getDirectoryMetaData();
        final File f = Paths.get(syncManager.getBaseDirectory().getAbsolutePath(), meta.getRelativePath()).toFile();

        f.delete();

        // replace local with remote
        syncManager.removeMetaData(meta.getRelativePath(), dirMetaData);
        dirMetaData.add(meta);
    }

    /**
     * Pulls a file from the server, decrypts it and stores it locally.
     *
     * @param meta a MetaData file
     */
    private void pullFile(final MetaData meta)
            throws ConnectionException, GeneralSecurityException,
            IOException, FileNotFoundException, UserNotLoggedInException,
            InvalidHttpStatusException {

        // so that the counting works correctly
        // do not call toPull.remove() within a foreach loop
        if (meta.isDeleted()) {
            toPullCount--;
        }

        final List<MetaData> dirMetaData = syncManager.getDirectoryMetaData();
        final Data data = syncManager.pull(meta);
        final MetaData pulledMeta = data.getMetaData();
        final Content pulledContent = data.getContent();

        // store file only if not delete
        if (pulledMeta.isDeleted()) return;

        final File file = Paths.get(syncManager.getBaseDirectory().toString(), pulledMeta.getRelativePath()).toFile();
        if (meta.isEncrypted()) {
            FileUtils.writeByteArrayToFile(file, pulledContent.getContent());
        } else {
            final SecretKey key = KeyManager.getInstance().decryptKey(pulledMeta);
            final byte[] dec = AES.decrypt(pulledContent.getContent(), key, new IvParameterSpec(pulledMeta.getIv()));
            FileUtils.writeByteArrayToFile(file, dec);
        }

        syncManager.removeMetaData(pulledMeta.getRelativePath(), dirMetaData);
        pulledMeta.setEncrypted(meta.isEncrypted());
        dirMetaData.add(pulledMeta);
    }

    /**
     * Check what files need to be added, updated and deleted.
     * <p/>
     * <p/>
     * Then the MetaData files will be stored in their respective lists, which decide what will be done to them.
     */
    private void checkAll()
            throws ConnectionException, UserNotLoggedInException {

        // obtain local and remote meta data
        List<MetaData> localMeta = syncManager.getDirectoryMetaData();
        List<MetaData> remoteMeta = new ArrayList<>();

        // since Arrays.asList doesn't work
        for (MetaData meta : syncManager.update()) {
            remoteMeta.add(meta);
        }

        // iterate over all local files
        for (MetaData local : localMeta) {
            // obtain the corresponding remote one
            MetaData remote = syncManager.getMetaData(local.getRelativePath(), remoteMeta);

            // if remote is null, local needs to be added
            if (remote == null) {
                toPush.add(local);
                continue;
            }

            // exclude this MetaData
            remoteMeta.remove(remote);

            // if remote is newer
            if (local.getLastModified() < remote.getLastModified()) {
                remote.setEncrypted(false);

                if (remote.isDeleted()) {
                    toRemoveLocal.add(remote);
                } else {
                    toPull.add(remote);
                }
                continue;
            }

            // if local is newer
            if (local.getLastModified() > remote.getLastModified()) {
                if (local.isDeleted()) {
                    toRemoveRemote.add(local);
                } else {
                    toPush.add(local);
                }
                continue;
            }
        }

        for (MetaData remote : remoteMeta) {
            remote.setEncrypted(false);
            toPull.add(remote);
        }

    }

    /**
     * Syncs from the actual files to the MetData stored locally. First all untracked will be added,
     * then all changed files will be updated and the no longer existing ones deleted from the MetaData.
     */
    private void syncLocal() {
        try {
            // update local meta data
            syncManager.addUntrackedFiles(syncManager.getBaseDirectory());
            syncManager.updateChangedFiles();
            syncManager.removeDeletedFiles();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Performs the local sync, then checkAll method and generates four lists containing entries to be pushed,
     * pulled, removed locally and removed remotely, which will then be done with their respective methods.
     */
    @Override
    public void syncAll() {

        try {
            toPush.clear();
            toPull.clear();
            toRemoveLocal.clear();
            toRemoveRemote.clear();

            syncLocal();
            checkAll();

            toPushCount = toPush.size();
            toPullCount = toPull.size();
            toRemoveLocalCount = toRemoveLocal.size();
            toRemoveRemoteCount = toRemoveRemote.size();

            for (MetaData file : toPush) {
                pushFile(file);
            }

            for (MetaData file : toPull) {
                pullFile(file);
            }

            for (MetaData file : toRemoveLocal) {
                removeLocal(file);
            }

            for (MetaData file : toRemoveRemote) {
                removeRemote(file);
            }

            view.showInformation("Synchronization complete.");
            view.showInformation("Pushed " + toPushCount + " files to the server.");
            view.showInformation("Pulled " + toPullCount + " files from the server.");
            view.showInformation("Removed " + toRemoveLocalCount + " files local.");
            view.showInformation("Removed " + toRemoveRemoteCount + " files remote.");

            syncManager.storeMetaDataToDisk();

        } catch (FileNotFoundException e) {
            view.showError("File does not exist '" + e.getFilePath() + "'");
        } catch (ConnectionException e) {
            view.showError("Server communication error");
        } catch (UserNotLoggedInException e) {
            view.showError("Currently no user is logged in, please sign in");
        } catch (FileOutOfDateException e) {
            view.showError("File '" + e.getFilePath() + "' is older than the server version");
        } catch (Exception e) {
            view.showError("Unknown error occurred: " + e.toString());
        }
    }

    /**
     * Flag if synchronisation controller has been successfully initialized.
     *
     * @return true if synchronisation controller has been initialized else false
     */
    @Override
    public boolean isInitialized() {

        return isInitialized;
    }
}
