package securecloud.controller;

import securecloud.controller.interfaces.IUserController;
import securecloud.model.UserManager;
import securecloud.model.entities.User;
import securecloud.model.exceptions.*;
import securecloud.view.interfaces.IView;

/**
 * User controller which offers function to register, login and logout a user.
 */
public class UserController implements IUserController {

    /**
     * Reference to interactive view.
     */
    private final IView view;

    /**
     * Default constructor.
     *
     * @param view view which interacts with the controller
     */
    public UserController(IView view) {

        this.view = view;
    }

    /**
     * Register a new user.
     *
     * @param user user which should be registered
     */
    @Override
    public void register(final User user) {

        try {
            UserManager.getInstance().register(user);
        } catch (UserRegistrationFailedException ex) {
            view.showError("User registration failed, email address already in use");
        } catch (InvalidHttpStatusException ex) {
            view.showError("Invalid HTTP status recognized: " + ex.toString());
        }
    }

    /**
     * Authenticate and login a user.
     *
     * @param user user which should be authenticated and logged in
     */
    @Override
    public void login(final User user) {

        try {
            UserManager.getInstance().login(user);
        } catch (InvalidHttpStatusException ex) {
            view.showError("Invalid HTTP status recognized: " + ex.toString());
        } catch (UserNotRegisteredException e) {
            view.showError("User '" + user.getEmail() + "' is not registered yet");
        } catch (UserLoginFailedException ex) {
            view.showError("Wrong password used, please try to login again");
        } catch (UserIsLockedException ex) {
            view.showError("User is locked due to many false attempts, please try again later");
        }
    }

    /**
     * Logout the currently logged in user.
     */
    @Override
    public void logout() {

        UserManager.getInstance().logout();
    }

    /**
     * Flag if any user is currently logged in.
     *
     * @return true if a user has signed in else false
     */
    @Override
    public boolean isUserLoggedIn() {
        return UserManager.getInstance().isUserLoggedIn();
    }
}
