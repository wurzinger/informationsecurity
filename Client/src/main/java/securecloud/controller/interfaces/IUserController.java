package securecloud.controller.interfaces;

import securecloud.model.entities.User;

/**
 * Necessary user functions which have to be provided.
 */
public interface IUserController {

    /**
     * Register a new user.
     *
     * @param user user which should be registered
     */
    void register(User user);

    /**
     * Authenticate and login a user.
     *
     * @param user user which should be authenticated and logged in
     */
    void login(User user);

    /**
     * Logout the currently logged in user.
     */
    void logout();

    /**
     * Flag if any user is currently logged in.
     *
     * @return true if a user has signed in else false
     */
    boolean isUserLoggedIn();
}
