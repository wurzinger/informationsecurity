package securecloud.controller.interfaces;

/**
 * Necessary functions to synchronize local and remote stored files.
 */
public interface ISyncController {

    /**
     * Initializes the SyncController for a certain directory.
     *
     * @param path directory in which the SyncController operates
     */
    void initialize(final String path);

    /**
     * Will reset the SyncController and all Classes used by it, making another initialize necessary.
     */
    void reset();

    /**
     * Encrypts all tracked files.
     */
    void encryptAll();

    /**
     * Decrypts all tracked files.
     */
    void decryptAll();

    /**
     * Performs the checkAll method and generates three lists containing entries to be added,
     * deleted and updated, which will then be done with their respective methods.
     */
    void syncAll();

    /**
     * Flag if synchronisation controller has been successfully initialized.
     *
     * @return true if synchronisation controller has been initialized else false
     */
    boolean isInitialized();
}
