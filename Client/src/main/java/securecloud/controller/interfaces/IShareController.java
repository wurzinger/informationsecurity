package securecloud.controller.interfaces;

/**
 * Necessary functions to share or unshare a file.
 */
public interface IShareController {

    /**
     * Share a file with another user.
     *
     * @param email    e-mail address of file participant
     * @param filePath path to file which should be shared
     * @return true if sharing was successful
     */
    boolean shareFile(final String email, final String filePath);

    /**
     * Revoke access right of a shared file.
     *
     * @param email    e-mail address of file participant
     * @param filePath path to shared file
     * @return true if unsharing was successful else fals
     */
    boolean unshareFile(final String email, final String filePath);
}
