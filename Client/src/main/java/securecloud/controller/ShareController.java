package securecloud.controller;

import securecloud.controller.interfaces.IShareController;
import securecloud.model.*;
import securecloud.model.entities.MetaData;
import securecloud.model.exceptions.*;
import securecloud.model.security.RSA;
import securecloud.view.interfaces.IView;

import java.io.File;
import java.nio.file.Paths;
import java.security.KeyPair;
import java.security.PublicKey;
import java.util.List;

/**
 * Share controller offers function to share and unshare a file with multiple users.
 */
public class ShareController implements IShareController {

    /**
     * Reference to interactive view.
     */
    private IView view = null;

    /**
     * Default constructor.
     *
     * @param view view which interacts with the controller
     */
    public ShareController(IView view) {

        this.view = view;
    }

    /**
     * Share a file with another user.
     *
     * @param email    e-mail address of file participant
     * @param filePath path to file which should be shared
     * @return true if sharing was successful else false
     */
    @Override
    public boolean shareFile(final String email, final String filePath) {

        boolean successful = false;

        try {
            if (!UserManager.getInstance().isUserLoggedIn())
                throw new UserNotLoggedInException();

            final SyncManager manager = SyncManager.getInstance();
            final File file = Paths.get(manager.getBaseDirectory().getPath(), filePath).toFile();
            if (!file.exists() || !file.isFile()) {
                throw new FileNotFoundException();
            }

            final String relativePath = manager.getRelativePath(file);
            final List<MetaData> metaData = manager.getDirectoryMetaData();
            final MetaData share = manager.getMetaData(relativePath, metaData);
            if(share == null) {
                throw new FileNotTrackedException();
            }

            final MetaData copy = new MetaData(share);

            final KeyStorage keyStorage = manager.getKeyStorage();
            final PublicKey publicKey = KeyManager.getInstance().fetchPublicKey(email);
            final KeyPair keyPair = keyStorage.loadAsymmetricKeys(UserManager.getInstance().getCurrentUser().getPassword());
            byte[] key = RSA.decrypt(copy.getKey(), keyPair.getPrivate());
            copy.setKey(RSA.encrypt(key, publicKey));
            copy.setParticipantEMail(email);

            ShareManager.getInstance().share(copy);

            successful = true;

        } catch (UserNotLoggedInException ex) {
            view.showError("Currently no user is logged in, please sign in");
        } catch (FileNotTrackedException ex) {
            view.showError("File is currently not tracked, please synchronise your directory first");
        } catch (PublicKeyNotFoundException ex) {
            view.showError("File participant hasn't uploaded a public key yet");
        } catch (FileAlreadyExistsException ex) {
            view.showError("Can't share '" + filePath + "', since user has a (shared) file with the same name");
        } catch (ConnectionException ex) {
            view.showError("Server communication error");
        } catch (FileNotFoundException ex) {
            view.showError("File '" + filePath + "'does not exist on the server, please synchronise your directory");
        } catch (java.io.FileNotFoundException ex) {
            view.showError(ex.getMessage());
        } catch (Exception ex) {
            view.showError("Unknown error occurred: " + ex.toString());
        }

        return successful;
    }

    /**
     * Revoke access right of a shared file.
     *
     * @param email    e-mail address of file participant
     * @param filePath path to shared file
     * @return true if unsharing was successful else fals
     */
    @Override
    public boolean unshareFile(final String email, final String filePath) {

        boolean successful = false;

        try {

            if (!UserManager.getInstance().isUserLoggedIn())
                throw new UserNotLoggedInException();

            final SyncManager manager = SyncManager.getInstance();
            final File file = Paths.get(manager.getBaseDirectory().getPath(), filePath).toFile();
            if (!file.exists()) {
                throw new FileNotFoundException();
            }

            final String relativePath = manager.getRelativePath(file);
            final List<MetaData> metaData = manager.getDirectoryMetaData();
            final MetaData share = manager.getMetaData(relativePath, metaData);
            final MetaData unshare = new MetaData(share);
            unshare.setParticipantEMail(email);

            ShareManager.getInstance().unshare(unshare);

            successful = true;

        } catch (UserNotLoggedInException ex) {
            view.showError("Currently no user is logged in, please sign in");
        } catch (UserNotAuthorizedException ex) {
            view.showError("You are not authorized to unshare the file '" + filePath + "', maybe you are not the owner of the it");
        } catch (ConnectionException ex) {
            view.showError("Server communication error");
        } catch (FileNotFoundException ex) {
            view.showError("File does not exist '" + filePath + "'");
        } catch (FileOutOfDateException ex) {
            view.showError("File '" + filePath + "' is older than the server version");
        } catch (Exception ex) {
            view.showError("Unknown error occurred: " + ex.toString());
        }

        return successful;
    }
}