import securecloud.controller.ShareController;
import securecloud.controller.SyncController;
import securecloud.controller.UserController;
import securecloud.view.ConsoleView;

// INSTALL
// http://www.oracle.com/technetwork/java/javase/downloads/jce8-download-2133166.html
// copy to {JAVA-HOME}\jre\lib\security

/*
user reminder:

login user@test.com asdf1234
init Client/data

 */

public class Application {

    public static void main(String args[]) {
        try {
            final ConsoleView view = new ConsoleView();
            final UserController userController = new UserController(view);
            final SyncController syncController = new SyncController(view);
            final ShareController shareController = new ShareController(view);
            view.setUserController(userController);
            view.setSyncController(syncController);
            view.setShareController(shareController);
            view.run();
        } catch (Exception ex) {
            System.err.println(ex.toString());
        }
    }
}