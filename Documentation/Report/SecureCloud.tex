%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Fabian Jeschko - fabian.jeschko@student.uibk.ac.at
%% Stefan Wurzinger - stefan.wurzinger@student.uibk.ac.at
%% Secure Cloud
%% 01.01.2016
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[703816-PR,hidelinks,11pt]{report}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}

\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage[top=3.5cm, bottom=3cm, left=3.5cm, right=3.5cm]{geometry}
\usepackage{setspace}
\usepackage{enumitem}
\usepackage{listings}

\graphicspath{{images/}}

\setstretch{1.4}
\setdescription{labelindent=\parindent}

\title{Secure file synchronization, sharing and cloud storage \\ Codename: SecureCloud}

\author{
Fabian Jeschko*\\
Stefan Wurzinger*\\
\\
{\small *firstname.lastname@student.uibk.ac.at}
}

\begin{document}
\linespread{1.5}
\maketitle

\begin{abstract}

Through the rise of cloud storage services documents, photos, music or in
general any electronic file can be stored and accessed from anywhere at anytime
through an Internet connection. They offer the opportunity to share files with
single users or user groups, hence they can be used as a collaborative working
environment. Popular cloud storage providers like Dropbox and iCloud don't
store their users data encrypted on their systems and can
therefore be accessed by villains if application security flaws are
exploited. Flaws in the application security can be fixed, but new ones might
be introduced at the same time, since a system will never be completly secure.
To avoid this completely, we developed an application that can be used to
store any electronic file encrypted in the cloud. The secrets we use to
encrypt/decrypt the data are kept private and are never transmitted to others
to obtain a Zero Knowledge application.

\end{abstract}

\vspace{4cm}
\begin{center}
\small
Source code and documentation available from:
https://bitbucket.org/wurzinger/informationsecurity
\end{center}


\newpage
\tableofcontents
\newpage

\section{Project Description}

The main idea of this project was to develop a server/client application which
is able to store files in the cloud and synchronize them among different
devices of a certain user. Additionally a user should be able to share a file
with other users to enable a collaborative working. Due to the fact that our
privacy is highly valuable, all the managed data should be stored and
transmitted in a secure manner, so that unauthorized people cannot access, use, or modify
it. Hence the information security properties confidentiality and integrity
have to be fulfilled and a Zero Knowledge application should be accomplished.
Furthermore the application should be easily usable. The primary focus
should be on data security and privacy as this is the objective of the course
\emph{Applied Information Security}.

% see also. http://lifehacker.com/the-best-cloud-storage-services-that-protect-your-priva-729639300

\section{Related Work}

Boxcryptor\footnote{https://www.boxcryptor.com/} is one of several existing
applications which allow the secure use of cloud storage services. It supports
cloud storage providers like \emph{Microsoft
OneDrive\footnote{https://onedrive.live.com}}, \emph{Google
Drive\footnote{https://www.google.com/drive/}},
\emph{Dropbox}\footnote{https://www.dropbox.com/}, and of course some others.
Boxcryptor creates a virtual drive on the users computer where sensitive data
can be stored. Any file within this drive will be automatically encrypted
before it is synchronized to the chosen cloud storage provider. To guarantee a
secure storage of files they take symmetric AES-256 and asymmetric RSA cipher
algorithms into account. They use AES-256 encryption to encrypt and decrypt a
file, whereas each file has its own unique and random key. 4096 bit RSA keys
(public and private key) are generated for each user and used for exchanging
file keys to enable file sharing. In addition Boxcryptor offers a feature which
allows file sharing with a group of users too. The secure exchange of a file
key between two users Alice and Bob works as follows:

\begin{enumerate}
\item Alice wants to share 'file.txt' with Bob and therefor requests Bob public
key from the Boxcryptor key server
\item Alice encrypts the file key of 'file.txt' with Bob's public key
\item Alice writes the new encrypted file key to the encrypted file
\item The cloud storage service provider synchronizes the modified encrypted file
\item Bob is able to decrypt the file key by using his private key
\item Bob uses the symmetric file key to decrypt the file 'file.txt'
\end{enumerate}
The exchange of a file key with a group of users looks quite the same but is
not further discussed as it is not part of the project. \cite{boxcryptor16secure}

Tresorit\footnote{https://tresorit.com} uses a similar approach to Boxcryptor
but they do not depend on cloud storage services as they provide their own
storage. All files that are uploaded to their storage are encrypted on the
client side with AES-256 in CFB mode. Encryption will be performed with a fresh
key for each file chosen by the client application. The integrity of the files
is protected with HMAC. File uploads and modifications are authenticated with
RSA-2048 signatures applied on SHA-512 hashes. TLS-protected channels are used
to exchange data between the client and server application. Once a file is
uploaded to the cloud it is added to a directory structure which reflects the
client-side directory structure. The remote directory consists of encrypted
files and directories and holds the related symmetric keys. A layered hierarchy
of directories is the result of this approach. The top of the hierarchy is the
root directory. Files can only be inserted into the remote directory if the a
user can provide the key the root directory is encrypted with. More precisely,
the key is provided by Tresorit's Agreement Module which manages the used keys.
The RSA-based Agreement Module consists of a set of pre-mastered secrets, one
for each user who is sharing the file, and of RSA public-key certificates of
the file contributors. The pre-mastered secrets are encrypted with the public
RSA key of the specific user. Thus, someone has to provide a private key to the
Agreement Module to decrypt the pre-master secret that is used to calculate the
symmetric key of the root directory to be able to access all stored files. The
symmetric key is calculated by applying HMAC to the user certificates as inputs
and with the pre-master secret as the key. \cite{tresorit16white}


\section{Model and Technologies}

In comparison to Boxcryptor we do not rely on any cloud storage provider for synchronization tasks and thus we had to implement our own client and server applications which are able to store, update, delete, request, and share files on demand. Figure \ref{fig:architecture} depicts the model and used technologies of the developed software. 

\begin{figure}[Ht]
\begin{center}
{\includegraphics[scale=0.6]{architecture.png}}    
\end{center}
\caption{Software model and used technologies.}
\label{fig:architecture}
\end{figure}

The project consists of three components, namely the server application, client
application and a database management system. Java 1.8 has been chosen as the
basic programming language due to the fact that many frameworks and libraries
are available which fulfill our needs and can be used to set up a secure
RESTful server and client application. Both applications depend on the
Spring\footnote{https://spring.io/} framework as it offers basic functionality
to build a RESTful web service and the corresponding REST client. A SSL
certificate is installed on the web server to enable a secure communication between the
client and server application. The free available database management system
MySQL\footnote{https://www.mysql.com/} is used to store all the data provided
by the different clients.

As for security, we decided to use AES for the symmetric encryption of files, RSA for the encryption of file keys, and SSL for the transmission of data. The reason being, that they were also used by Boxcryptor and Tresorit and are the most prominent and secure encryption algorithms. 

The Advanced Encryption Standard (AES), also known as Rijndael, is the successor of the Data Encryption Standard (DES), and since its adoption worldwide used as a de facto cryptographic standard by banks, administrations and industry. \cite{daemen13aes} 

The RSA (Rivest, Shamir, Adleman) cipher algorithm is based on a mathematical function that needs two keys to work. A private and a public key, where the private key is never shared with anyone else, but the public key can be known by anybody without problems. Data encrypted with the private key can only be decrypted by the public key and vice versa. That is the reason why it is called an asymmetrical encryption algorithm. Compared to symmetric encryption algorithms, asymmetric encryption algorithms are rather slow and should only be used to encrypt small data (e.g. symmetric keys). \cite{wardlaw00rsa}

SSL (Secure Sockets Layer) is a security standard used by servers to establish a secure link to browsers (clients). A web server needs a certificate and an asymmetric key pair (in our case a RSA key pair) to provide a SSL connection. The public key is placed in the certificate which has to be signed by a trusted certificate authority. The server offers the certificate to users if they request secured pages. The browser of the client checks if the certificate has been signed by a known trustworthy certificate authority and will then negotiate an algorithm and keys with the server to establish a secure transmission channel. \cite{mckinley03ssl}


\section{Functionality}

As already stated the software is capable of several functions which are covered in this section. Figure \ref{fig:basicworkflow} illustrates how the application should/can be treated and furthermore gives an overview of the most important functions which will be discussed in more detail in their respective sections. 

\begin{figure}[Ht]
\begin{center}
{\includegraphics[scale=0.55]{basic-workflow.png}}    
\end{center}
\caption{Application treatment.}
\label{fig:basicworkflow}
\end{figure}

It should be noted that the client application is implemented as a console application which is able to execute the following commands:

\begin{description}
\singlespacing

\item[help] -- display all available commands
\item[register] -- create a new user account
\item[login] -- once registered, the user can sign in
\item[init] -- initialize the application to work with a given directory
\item[sync] -- upload/download file changes to/from the web server
\item[encrypt] -- encrypt all files locally
\item[decrypt] -- decrypt all files locally
\item[share] -- share a file with a given user to enable collaborative working
\item[unshare] -- revoke shared file access rights
\item[logout] -- sign out
\item[exit] -- close the application

\end{description}

\subsection{Registration / Login}

As Figure \ref{fig:basicworkflow} demonstrates, a user has to create an account
if he or she wants to use the application. A new account can be created by
executing the ``register'' command. The user needs to specify an email address
and a password which will be later used as authentication credentials. To
prevent rainbow table attacks the password is hashed with the PBKDF2 algorithm
by using the options HMAC-SHA256, 1000 iterations and 192 bits key length as
well as a 32 bytes base 64 encoded salt value. Afterwords the new inferred
password will be transmitted with the according salt value and the associated
email address to the web server. The web server validates the transmitted
values and checks if the provided email address is not already in use. After a
successful registration the user is able to sign up to the application and use
it. As already mentioned the user needs to provide an email address plus a
password for authentication. The client requests the stored salt value
and calculates with the same hashing algorithm the inferred password and sends
it together with the email address to server, which verifies the user credentials. The plain
password is never sent to server due to security reasons, even if it is secured
through the SSL connection. A user has three attempts to sign up, after
exceeding this number the user account will be banned for 15 minutes. This
mechanism should prohibit villains to guess passwords of other users. Once a
user is logged in, a directory has to be selected which should be synchronized
with the web server. This process is called initialization.

\subsection{Initialization}

Before the application is usable it has to be initialized. Initialization has
to occur after each login by calling the ``$init\  \langle dir \rangle$'',
where \emph{dir} is the base directory that will be securely synchronized with
the server. The client application creates a hidden sub folder within the
specified base directory that consists of a metadata file and an asymmetric key
pair. The metadata file consists of additional information for all
synchronized files. The metadata file is used for synchronization purposes and is discussed in more detail in Section \ref{sub:synchronization}. The asymmetric keys are generated with the RSA algorithm and have a length of 4096 bits. These keys are used to encrypt and decrypt file keys. The public key file is stored in plain text on the local
disk whereas the private key is encrypted with the inferred user password.
Hence the private key file is unusable without a password which protects you
from key theft. The public key of a user is furthermore sent to the web server and can afterwords requested by other users to enable a secure file sharing mechanism. If the specified base directory has been previously initialized as a secure directory the client application is going to load the metadata file and the asymmetric keys to be able to manage all the stored data inside that directory. 
A user can synchronize the sensitive data with multiple devices,
only the asymmetric keys have to be copied to the corresponding folders on the
other devices. After initializing a directory the client application can track
all the file changes and can synchronize them with the web server. Tracked
files may be shared with other users for a collaborative working.

\subsection{Synchronization}
\label{sub:synchronization}

Synchronization occurs once a user calls the ``sync'' command and is split into three phases, called \emph{Local Synchronization}, \emph{Differencing} and \emph{Remote Synchronization}, executed in that order. In the local synchronization phase all local file changes are detected. Afterwords the differencing phase takes place and compares the locally tracked files with the files from the server and determines which files should be added, modified or deleted locally or remotely. Lastly, the remote synchronization phase is responsible for uploading and downloading the files specified by the differencing process. The whole process is depicted in Figure \ref{fig:sync} and will be elucidated in more detail, but before digging deeper into it, the previously mentioned metadata file needs to be discussed to understand the synchronization process.

\begin{figure}[Ht]
\begin{center}
{\includegraphics[scale=0.55]{sync-timeline.png}}    
\end{center}
\caption{Workflow of the synchronization process.}
\label{fig:sync}
\end{figure}

\subsubsection*{Metadata}

The metadata file contains, as the name suggests, metadata information about all files which belong to the specified base directory or to one of its subdirectories. The metadata for each file consists of several attributes which are used to identify file changes between local and remote files and are described below.


\begin{description}

\item[Relative path:] Due to the fact that multiple files with the same name can exist in different subdirectories the relative path, regarding to the base directory, has been chosen as a unique file identifier on the client side.

\item[Last modified time and file hash:] The last modified time and the hash of the encrypted file are used to determine if a new file should be tracked or if a file has been changed locally. It might be that the file has been changed and reverted, retaining the hash but not the timestamp. In that case the metadata file is going to be uploaded to the server to stay synchronized.

\item[Owner and participant email:] The owner and participant mail attributes
are used to indicate file permissions of users. The owner mail indicates the
owner of a file whereas the participant mail describes to which user a metadata
file belongs to.

\item[Initialization vector and symmetric file key:] The initialization vector and the symmetric file key are used for encryption and decryption purposes. The file key (AES key) is encrypted with the public RSA key (recall: created during initialization) and hence not stored in plain text on the file system.

\item[Encrypted flag:] The flag is used to indicate if the local files are currently encrypted or available in plain text. This is crucial to avoid encrypting the file multiple times or decrypting a decrypted file.

\item[Deleted flag:] Used to verify if a file has been or has to be deleted. 

\end{description}


\subsubsection*{Local Synchronization}

The local synchronization phase updates the metadata file to properly reflect the file states (new, modified, deleted) of the secured folder. To do that, the secured folder and all its subfolders are recursively traversed. For each file that is not yet tracked by the application a fresh and random AES-256 key (encrypted with public key) will be created and inserted with some additional metadata information into the metadata file. Afterwords, the program checks if any of the files were modified or deleted, if so, the metadata file will be updated. The metadata information about a deleted file is kept, due to synchronization reasons, but the file is marked as deleted and unnecessary information like the symmetric file key and initialization vector used for encryption purposes are removed. 

\subsubsection*{Differencing}

After the local synchronization phase, the differencing process takes over and
requests the metadata information of the remotely stored files, which
represents the state of files on the server. The local and remote metadata are
compared against each other to determine which files have to be pushed, pulled
or deleted. A file that is present in the local metadata file but not in the
remote one has been newly added by a user and hence needs later to be uploaded
to the web server. Files that are present in the remote metadata but not in the
local one have never been pulled from the server and will later on be
downloaded.
These were the obvious cases. If there is a file listed in the remote and local metadata the time of their last change is considered to check which version is newer. If the local version is newer, the file will either be uploaded to or deleted on the server. Otherwise the file will be downloaded from the server later on. The calculated actions are handed over to the remote synchronization phase which uploads, downloads or deletes the changed files.

\subsubsection*{Remote Synchronization}

The remote synchronization process encrypts all the files that have to be pushed (if they are already encrypted, this step will be skipped) and calls the corresponding REST service offered by the web server (REST POST call). The encrypted file enriched with the new metadata information is sent to the server. The server updates the remote file and metadata before returning the metadata 
again with any changes that it might have made (assigning an id for example). The metadata file of the client will then be replaced with the one the server returned.

Then the files that were marked for downloading are obtained by requesting the specified files (REST GET request). The request body consists of the metadata for this specific file. The server returns the requested file with the corresponding metadata. The local metadata will be overwritten with the remote one and the received encrypted file is going to be decrypted and written to the local file system.

All files that should be deleted are going to be deleted on the server by providing the metadata of that files (REST DELETE call). The server will then delete the file if the user is privileged to do so (owner) and return the possibly new metadata.

The last part is the removal of local files that were marked to be deleted. This is simply done by removing them from the user's file system, completing the synchronization process.

The user will be informed by the system if something went wrong or if everything worked properly. Due to the use of the available HTTP status codes by the RESTful server, the client application is able to recognize different exceptions and to handle them appropriately.

\subsection{Sharing}
The application can be used as a collaborative working environment to exchange
and edit information. Users are able to share their (tracked) data with others
by executing the ``share'' command with the email address of the new file
participant and the path to the shared file as parameters. A secure
collaborative working between two or multiple users is ensured through data
encryption and a secure file key exchange mechanism that is represented in
Figure \ref{fig:shareworkflow}. As the figure shows there are two users
involved in the sharing process whereas Alice is the owner of a file and wants
to share it with another user called Bob by executing the above mentioned
``share'' command. All the files from Alice are already synchronized with the
web server and are thus securely stored (recall: encrypted with a symmetric
key). Due to that fact Bob needs a key to decrypt the shared file to be able to
read and modify it. Hence a key exchange between the file owner and the file
participant is required. To transmit the file key Alice's client application
requests the public RSA key of Bob from the web server. The received public key
is used to encrypt the symmetric shared file key so that Bob is the only person
who can decrypt the encrypted symmetric file key. Afterwards the encrypted key
enriched with some metadata is sent to the web server which grants Bob access
to the shared file. Bob receives the shared file and the corresponding file key
from the server after the next synchronization. Bob can decrypt the encrypted
symmetric file key with his own private RSA key. The resulting key can then be
used to decrypt the shared file. Bob is now able to read and update the file.

A file owner can revoke file access rights by calling the command ``unshare''
with the same parameters as used for the file sharing. A file participant can
simply revoke the own rights from a shared file by deleting the file locally.

\begin{figure}[Ht]
\begin{center}
{\includegraphics[scale=0.55]{sharing-workflow.png}}    
\end{center}
\caption{File sharing process between two users.}
\label{fig:shareworkflow}
\end{figure}

\subsection{Encryption and Decryption}

Files can be encrypted and decrypted after a user has signed up to the application and successfully initialized it. A file is encrypted by obtaining the corresponding symmetric file key for it out of the metadata file. The symmetric key is encrypted with the public key of the current user. So the private key is loaded from the file system, which requires a user password to decrypt the private key. Then through RSA decryption the symmetric key is obtained. For the file encryption the AES algorithm with a key size of 256 bits is used, together with cipher block chaining (CBC) and PKS5 padding. That’s the reason why an initialization vector (IV) needs to be stored in the metadata file for each tracked file. With the symmetric key and the IV the file can then be encrypted. The decryption of a file works exactly the same as encryption. It is important to note that encryption and decryption change the last modified timestamp that each file has, which is used in the synchronization process, so it is set to the one prior to encryption/decryption.


\section{Future Work}

As for possible improvements, there would be a few. First of all, changing the user
password as well as refresh a file key is not yet possible and a crucial feature that is missing.  Another enhancement would be the ability to reset the users password through commonly known techniques. A well known example are security questions or sending an email with a randomly generated password. Users should also be able to create groups of multiple users, with which they can share their files too. Once groups are available, a user customizable right system can be implemented for groups and individual users, to allow read/write access. A further feature could be considered that allows majority encryption. Basically a user would then need a certain amount out of all participating users keys to be able to decrypt a file. The upload of huge files should also be optimized, as a file upload can take a long time and might probably never finish due to the fact that the internet might be too slow and the application is not running 24 hours a day. So splitting huge files into chunks and uploading them separately to the server should be implemented as well. To improve the collaborative working a merge tool should be provided by the system to be able to merge two file versions two prevent information loss.

From the technical side, there are some missing parts too. Right now, users need
to import both the private and public key to set up the application on another
computer/directory, even though the public key is stored on the server and could be automatically fetched on new environments. The application still needs to be tested in terms of performance with many users and the trade-off between security and performance. Proper security testing is also missing as of yet. The technologies are secure for sure, but there might be mistakes in the implementation or the server might
be vulnerable to certain attacks (DDOS, spamming garbage files to the server).

% References
\bibliographystyle{ieeetr}
\bibliography{SecureCloud}

\end{document}


