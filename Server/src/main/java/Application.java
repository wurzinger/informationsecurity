import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import securecloud.config.ApplicationSecurityConfig;

/**
 * Main entry point to start the Spring server application.
 */
@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan("securecloud")
@EntityScan("securecloud.model.entities")
@EnableJpaRepositories("securecloud")
@Import({ ApplicationSecurityConfig.class })
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
