package securecloud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import securecloud.model.entities.Data;
import securecloud.model.entities.MetaData;
import securecloud.model.entities.User;
import securecloud.model.exceptions.FileOutOfDateException;
import securecloud.model.exceptions.UnknownOwnerException;
import securecloud.model.exceptions.UserNotAuthorizedException;
import securecloud.model.services.FileService;
import securecloud.model.services.UserService;

import java.io.FileNotFoundException;
import java.nio.file.FileAlreadyExistsException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * REST controller to store, update, fetch, share and unshare files.
 */
@RestController
public class FileController {

    @Autowired
    private FileService fileService;

    @Autowired
    private UserService userService;

    /**
     * Fetch all files of a registered and authenticated user.
     * Basic authentication is required to access these service.
     * Accessible through the URL '/files'.
     *
     * @return HttpStatus.OK and all stored files which belong to the authenticated user
     */
    @RequestMapping(value = "/files", method = RequestMethod.GET)
    public ResponseEntity<Data[]> loadAllFiles() {

        final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        final User user = userService.loadUser(auth.getName());

        final List<Data> dataList = new ArrayList<>();
        for (final MetaData metaData : fileService.loadMetaDataByParticipant(user)) {
            final Data data = new Data();
            data.setContent(metaData.getContent());
            data.setMetaData(metaData);
            dataList.add(data);
        }

        return new ResponseEntity<>(dataList.toArray(new Data[dataList.size()]), HttpStatus.OK);
    }

    /**
     * Request a file by providing a meta data object.
     * Meta data object has to provide a file owner, file participant, and a relative file path.
     * Basic authentication is required to access these service.
     * Accessible through the URL '/file'.
     *
     * @param meta meta data object
     * @return HttpStatus.OK and the file object if requested file has been found
     * HttpStatus.NOT_FOUND if requested file doesn't exist
     */
    @RequestMapping(value = "/file", method = RequestMethod.POST)
    public ResponseEntity<Data> loadFile(@RequestBody MetaData meta) {

        final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        final User user = userService.loadUser(auth.getName());
        final User owner = userService.loadUser(meta.getOwnerEMail());

        final HttpStatus status;
        final Data data;
        final MetaData found = fileService.loadMetaData(meta.getRelativePath(), owner, user);

        if (found != null) {
            data = new Data();
            data.setContent(found.getContent());
            data.setMetaData(found);
            status = HttpStatus.OK;
        } else {
            data = null;
            status = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(data, status);
    }

    /**
     * Store/update an encrypted file with its meta data.
     * Basic authentication is required to access these service.
     * Accessible through the URL '/file/store'.
     *
     * @param file file to store
     * @return HttpStatus.OK and the stored meta data object if storing was successful
     * HttpStatus.CONFLICT if a newer file version exists on the server
     * HttpStatus.FAILED_DEPENDENCY if the file owner can not be found
     * HttpStatus.UNAUTHORIZED if the a shared file has been deleted by the owner and participant wants to update the file
     */
    @RequestMapping(value = "/file/store", method = RequestMethod.POST)
    public ResponseEntity<MetaData> storeFile(@RequestBody Data file) {

        final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        final User user = userService.loadUser(auth.getName());

        HttpStatus status;
        MetaData metaData = null;

        try {
            metaData = fileService.storeFile(user, file);
            status = HttpStatus.OK;
        } catch (FileOutOfDateException e) {
            status = HttpStatus.CONFLICT;
        } catch (UnknownOwnerException e) {
            status = HttpStatus.FAILED_DEPENDENCY;
        } catch (UserNotAuthorizedException e) {
            status = HttpStatus.UNAUTHORIZED;
            metaData = e.getMeta();
        }

        return new ResponseEntity<>(metaData, status);
    }

    /**
     * Delete an already stored file.
     * Basic authentication is required to access these service.
     * Accessible through the URL '/file/delete'.
     *
     * @param meta meta data information about the file which should be deleted
     * @return HttpStatus.OK if deleting was successful
     * HttpStatus.CONFLICT if a newer file version exists on the server
     */
    @RequestMapping(value = "/file/delete", method = RequestMethod.POST)
    public ResponseEntity<Long> deleteFile(@RequestBody MetaData meta) {

        final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        final User user = userService.loadUser(auth.getName());

        HttpStatus status;
        Long updateTime = null;

        try {
            updateTime = fileService.deleteFile(user, meta);
            status = HttpStatus.OK;
        } catch (FileOutOfDateException e) {
            status = HttpStatus.CONFLICT;
        }

        return new ResponseEntity<>(updateTime, status);
    }

    /**
     * Fetch all meta data files of a registered and authenticated user.
     * Basic authentication is required to access these service.
     * Accessible through the URL '/meta'.
     *
     * @return HttpStatus.OK and all stored meta data files which belong to the authenticated user
     */
    @RequestMapping(value = "/meta", method = RequestMethod.GET)
    public ResponseEntity<MetaData[]> loadAllMetaData() {

        final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        final User user = userService.loadUser(auth.getName());
        final List<MetaData> files = fileService.loadMetaDataByParticipant(user);
        return new ResponseEntity<>(files.toArray(new MetaData[files.size()]), HttpStatus.OK);
    }

    /**
     * Share an owned file with another user.
     * Basic authentication is required to access these service.
     * Accessible through the URL '/share'.
     *
     * @param meta meta data information about the file which should be shared
     * @return HttpStatus.OK and shared meta data object if sharing was successful
     * HttpStatus.FORBIDDEN if file has already been shared with the user or user owns already such a file (has a file with the same name)
     * HttpStatus.NOT_FOUND if owner doesn't have such a file
     */
    @RequestMapping(value = "/share", method = RequestMethod.POST)
    public ResponseEntity<MetaData> shareFile(@RequestBody MetaData meta) {

        final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        final User owner = userService.loadUser(auth.getName());
        final User participant = userService.loadUser(meta.getParticipantEMail());

        HttpStatus status;
        MetaData value = null;

        try {
            // check if participant, does not already have such a file
            MetaData existing = fileService.loadMetaData(meta.getRelativePath(), participant);
            if (existing != null && !existing.isDeleted()) {
                throw new FileAlreadyExistsException(meta.getRelativePath());
            }

            final MetaData original = fileService.loadMetaData(meta.getRelativePath(), owner, owner);
            if (original == null || original.isDeleted()) {
                throw new FileNotFoundException();
            }

            meta.setOwner(owner);
            meta.setParticipant(participant);
            meta.setContent(fileService.loadContent(meta.getRelativePath(), owner, owner));
            meta.setLastModified(new Date().getTime());
            if (existing != null)
                meta.setId(existing.getId());

            value = fileService.storeMetaData(meta);
            status = HttpStatus.OK;

        } catch (FileAlreadyExistsException ex) {
            status = HttpStatus.FORBIDDEN;
        } catch (FileNotFoundException e) {
            status = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(value, status);
    }

    /**
     * Delete/remove grants from a shared file.
     * Basic authentication is required to access these service.
     * Accessible through the URL '/unshare'.
     *
     * @param meta meta data information about the file which should be unshared
     * @return HttpStatus.OK if unsharing was successful
     * HttpStatus.CONFLICT if a newer file version exists on the server
     * HttpStatus.UNAUTHORIZED if a user is not allowed to unshare file
     */
    @RequestMapping(value = "/unshare", method = RequestMethod.POST)
    public ResponseEntity<?> unshareFile(@RequestBody MetaData meta) {

        final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        final User user = userService.loadUser(auth.getName());

        HttpStatus status;

        try {
            fileService.unshareFile(user, meta);
            status = HttpStatus.OK;
        } catch (UserNotAuthorizedException e) {
            status = HttpStatus.UNAUTHORIZED;
        } catch (FileOutOfDateException e) {
            status = HttpStatus.CONFLICT;
        }

        return new ResponseEntity<>(status);
    }
}
