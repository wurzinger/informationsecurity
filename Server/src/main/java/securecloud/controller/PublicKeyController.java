package securecloud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import securecloud.model.entities.PublicKey;
import securecloud.model.entities.User;
import securecloud.model.services.PublicKeyService;
import securecloud.model.services.UserService;
import securecloud.model.exceptions.PublicKeyNotFoundException;

import javax.validation.Valid;

/**
 * REST controller to store and request public keys of users.
 */
@RestController
public class PublicKeyController {

    @Autowired
    private PublicKeyService keyService;

    @Autowired
    private UserService userService;

    /**
     * Register a new RSA public key of a registered user by providing a valid PublicKey object.
     * PublicKey object is valid if it provides a RSA modulus and exponent.
     * Basic authentication is required to access these service.
     * Accessible through the URL '/pkey/store'.
     *
     * @param key a valid PublicKey Object
     * @return HttpStatus.OK if storage was successful
     * HttpStatus.BAD_REQUEST if provided PublicKey object is not valid
     */
    @RequestMapping(value = "/pkey/store", method = RequestMethod.POST)
    public ResponseEntity<?> store(@RequestBody @Valid PublicKey key) {

        final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        key.setUser(userService.loadUser(auth.getName()));
        keyService.store(key);
        return new ResponseEntity(HttpStatus.OK);
    }

    /**
     * Fetch a RSA public key of a registered user by providing an e-mail address of a user.
     * Basic authentication is required to access these service.
     * Accessible through the URL '/pkey/fetch'.
     *
     * @param email e-mail address of a user
     * @return HttpStatus.OK and public key object if user and public key has been found
     * HttpStauts.NOT_FOUND if user doesn't exist or hasn't uploaded any public key
     */
    @RequestMapping(value = "/pkey/fetch", method = RequestMethod.POST)
    public ResponseEntity<PublicKey> load(@RequestBody String email) {

        HttpStatus status;
        PublicKey key = null;

        try {
            final User user = userService.loadUser(email);
            key = keyService.load(user);
            status = HttpStatus.OK;
        } catch (PublicKeyNotFoundException ex) {
            status = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(key, status);
    }
}
