package securecloud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import securecloud.model.entities.User;
import securecloud.model.services.UserService;
import securecloud.model.exceptions.UserAlreadyExistsException;
import securecloud.model.exceptions.UserAuthenticationException;
import securecloud.model.exceptions.UserDoesNotExistException;
import securecloud.model.exceptions.UserIsLockedException;

/**
 * REST controller to register and verify users.
 */
@RestController
public class UserController {

    @Autowired
    private UserService service;

    /**
     * Register a new user by providing a valid user account object.
     * User account object has to provide a first name, last name,
     * hashed password, e-mail and a password salt.
     * Accessible through the URL '/user/register'.
     *
     * @param account a valid user account
     * @return HttpStatus.OK if registering was successful
     * HttpStatus.CONFLICT if user already exists
     * HttpStatus.BAD_REQUEST if provided user account object is not valid
     */
    @RequestMapping(value = "/user/register", method = RequestMethod.POST)
    public ResponseEntity<?> register(@RequestBody User account) {

        HttpStatus status = HttpStatus.OK;

        try {
            service.register(account);
        } catch (UserAlreadyExistsException ex) {
            status = HttpStatus.CONFLICT;
        }

        return new ResponseEntity(status);
    }

    /**
     * Verify a user by providing an e-mail and a hashed password.
     * Accessible through the URL '/user/login'.
     *
     * @param account user account with e-mail and hashed password set
     * @return HttpStatus.OK and user object if login was successful
     * HttpStatus.NOT_FOUND if user is not registered
     * HttpStatus.LOCKED if user has been locked due to many false attempts
     * HttpStatus.UNAUTHORIZED if user authentication failed
     */
    @RequestMapping(value = "/user/login", method = RequestMethod.POST)
    public ResponseEntity<User> login(@RequestBody User account) {

        User user = null;
        HttpStatus status;

        try {
            user = service.login(account.getEmail(), account.getPassword());
            status = HttpStatus.OK;
        } catch (UserDoesNotExistException ex) {
            status = HttpStatus.NOT_FOUND;
        } catch (UserIsLockedException ex) {
            status = HttpStatus.LOCKED;
        } catch (UserAuthenticationException e) {
            status = HttpStatus.UNAUTHORIZED;
        }

        return new ResponseEntity<>(user, status);
    }

    /**
     * Request the salt value of a users hashed password.
     * Accessible through the URL '/user/salt'.
     *
     * @param email e-mail address of a registered user
     * @return HttpStatus.OK and salt value if user exists
     * HttpStatus.NOT_FOUND if user is not registered
     */
    @RequestMapping(value = "/user/salt", method = RequestMethod.POST)
    public ResponseEntity<String> salt(@RequestBody String email) {

        HttpStatus status;
        String salt = null;

        try {
            salt = service.getSalt(email);
            status = HttpStatus.OK;
        } catch (UserDoesNotExistException ex) {
            status = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(salt, status);
    }
}
