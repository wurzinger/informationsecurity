package securecloud.model.exceptions;

/**
 * This exception will be thrown if a user tried to login with the wrong credentials to many times.
 */
public class UserIsLockedException extends Exception {

}
