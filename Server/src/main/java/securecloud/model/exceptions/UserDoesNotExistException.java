package securecloud.model.exceptions;

/**
 * This exception will be thrown if a user is not registered.
 */
public class UserDoesNotExistException extends Exception {

}
