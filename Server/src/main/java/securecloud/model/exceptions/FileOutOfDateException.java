package securecloud.model.exceptions;

/**
 * This exception will be thrown if a user wants to update a file
 * for which a newer version exists.
 */
public class FileOutOfDateException extends Exception {

}
