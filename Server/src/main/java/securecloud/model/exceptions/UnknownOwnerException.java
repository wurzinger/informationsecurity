package securecloud.model.exceptions;

/**
 * This exception will be thrown if the owner of a shared file is unknown
 */
public class UnknownOwnerException extends Exception {

}
