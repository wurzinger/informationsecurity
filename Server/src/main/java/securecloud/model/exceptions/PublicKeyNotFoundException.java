package securecloud.model.exceptions;

/**
 * This exception will be thrown if public key of a user doesn't exist.
 */
public class PublicKeyNotFoundException extends Exception {

}
