package securecloud.model.exceptions;

import securecloud.model.entities.MetaData;

/**
 * This exception will be thrown if a user wants to execute an action
 * without any permission.
 */
public class UserNotAuthorizedException extends Exception {

    private final MetaData meta;

    public UserNotAuthorizedException() {
        this.meta = null;
    }

    public UserNotAuthorizedException(MetaData meta) {
        this.meta = meta;
    }

    public MetaData getMeta() {
        return meta;
    }
}
