package securecloud.model.exceptions;

/**
 * This exception will be thrown if a new user wants to register
 * with an already existing e-mail address.
 */
public class UserAlreadyExistsException extends Exception {

    public UserAlreadyExistsException(final String message) {
        super(message);
    }
}
