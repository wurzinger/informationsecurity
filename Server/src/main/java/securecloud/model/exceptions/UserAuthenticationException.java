package securecloud.model.exceptions;

/**
 * This exception will be thrown if a user authentication failed.
 */
public class UserAuthenticationException extends Exception {
}
