package securecloud.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import securecloud.model.entities.converter.BigIntegerConverter;
import securecloud.model.interfaces.IRSAPublicKey;
import securecloud.model.validators.ValidRSAPublicKey;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigInteger;

/**
 * Database entity to store public key of a registered user.
 * Stored in the table pkeys.
 */
@Entity
@Table(name = "pkeys")
@ValidRSAPublicKey
public class PublicKey implements IRSAPublicKey {

    /**
     * Primary key (id) of public key.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    /**
     * Owner of the public key.
     */
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @JsonIgnore
    private User user;

    /**
     * Modulus value of the RSA public key.
     */
    @NotNull
    @Convert(converter = BigIntegerConverter.class)
    @Column(columnDefinition = "blob")
    private BigInteger modulus;

    /**
     * Exponent value of the RSA public key.
     */
    @NotNull
    @Convert(converter = BigIntegerConverter.class)
    @Column(columnDefinition = "blob")
    private BigInteger exponent;

    /**
     * Default JPA constructor.
     */
    public PublicKey() {

    }

    /**
     * Default JPA constructor.
     */
    public PublicKey(long id) {
        this.id = id;
    }

    /**
     * Get the owner of the RSA public key.
     *
     * @return owner of public key
     */
    public User getUser() {
        return user;
    }

    /**
     * Set the owner of the public key.
     *
     * @param user owner of public key
     */
    public void setUser(final User user) {
        this.user = user;
    }

    /**
     * Get modulus of RSA public key.
     *
     * @return modulus
     */
    public BigInteger getModulus() {
        return modulus;
    }

    /**
     * Set modulus of RSA public key.
     *
     * @param modulus modulus of RSA public key
     */
    public void setModulus(BigInteger modulus) {
        this.modulus = modulus;
    }

    /**
     * Get exponent of RSA public key.
     *
     * @return exponent
     */
    public BigInteger getExponent() {
        return exponent;
    }

    /**
     * Set exponent of RSA public key.
     *
     * @param exponent exponent of RSA public key
     */
    public void setExponent(BigInteger exponent) {
        this.exponent = exponent;
    }
}
