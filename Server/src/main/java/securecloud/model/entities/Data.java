package securecloud.model.entities;

/**
 * Helper class to associate a file content with its meta data.
 */
public class Data {

    /**
     * File meta data.
     */
    private MetaData metaData;

    /**
     * File content.
     */
    private Content content;

    /**
     * Get file meta data.
     *
     * @return file meta data
     */
    public MetaData getMetaData() {
        return metaData;
    }

    /**
     * Set file meta data.
     *
     * @param metaData meta data
     */
    public void setMetaData(MetaData metaData) {
        this.metaData = metaData;
    }

    /**
     * Get file content.
     *
     * @return file content
     */
    public Content getContent() {
        return content;
    }

    /**
     * Set file content.
     *
     * @param content content
     */
    public void setContent(Content content) {
        this.content = content;
    }
}
