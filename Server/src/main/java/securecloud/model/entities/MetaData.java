package securecloud.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Database entity to store meta data about a file.
 * Composite of relative path, owner and participant of a file needs to be unique.
 */
@Entity
@Table(name = "meta_data",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"relative_path", "owner_id", "participant_id"})
        }
)
public class MetaData {

    /**
     * Primary key (id) of meta data.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    private long id;

    /**
     * Relative path of locally stored file.
     */
    @NotNull
    @NotEmpty
    @Column(name = "relative_path", length = 1024)
    private String relativePath;

    /**
     * Owner of the file.
     */
    @OneToOne
    @JoinColumn(name = "owner_id", referencedColumnName = "id")
    @JsonIgnore
    private User owner;

    /**
     * Shared file participant.
     * If file is not shared participant equals owner.
     */
    @OneToOne
    @JoinColumn(name = "participant_id", referencedColumnName = "id")
    @JsonIgnore
    private User participant;

    /**
     * ID of owner.
     */
    @Column(name = "owner_id", insertable = false, updatable = false)
    @JsonIgnore
    private long ownerId;

    /**
     * ID of participant
     */
    @Column(name = "participant_id", insertable = false, updatable = false)
    @JsonIgnore
    private long participantId;

    /**
     * E-mail address of owner.
     */
    @Transient
    private String ownerEMail;

    /**
     * E-mail address of participant.
     */
    @Transient
    private String participantEMail;

    /**
     * Reference to file content.
     */
    @OneToOne
    @JoinColumn(name = "contentId", referencedColumnName = "id", nullable = true)
    @JsonIgnore
    private Content content;

    /**
     * ID of file content.
     */
    @Column(name = "contentId", insertable = false, updatable = false, nullable = true)
    @JsonIgnore
    private Long contentId;

    /**
     * Encryption and decryption initialization vector.
     */
    @Column(columnDefinition = "blob", nullable = true)
    private byte[] iv;

    /**
     * Hash value of encrypted file content.
     */
    @Column(name = "`hash`", columnDefinition = "blob", nullable = true)
    private byte[] hash;

    /**
     * Flag if file has been deleted.
     */
    private boolean isDeleted;

    /**
     * Last update time of file content.
     */
    private long lastModified;

    /**
     * Encrypted key to decrypt file content.
     */
    @Column(name = "`key`", columnDefinition = "blob", nullable = true)
    private byte[] key;

    /**
     * Default JPA constructor.
     */
    public MetaData() {
    }

    /**
     * Default JPA constructor.
     */
    public MetaData(long id) {
        this.id = id;
    }

    /**
     * Get the encryption/decryption initialization vector.
     *
     * @return initalization vector
     */
    public byte[] getIv() {
        return iv;
    }

    /**
     * Set the encryption/decryption initialization vector.
     *
     * @param iv initialization vector
     */
    public void setIv(byte[] iv) {
        this.iv = iv;
    }

    /**
     * Get encrypted file key.
     *
     * @return encrypted key
     */
    public byte[] getKey() {
        return key;
    }

    /**
     * Set encrypted file key.
     *
     * @param key file key
     */
    public void setKey(byte[] key) {
        this.key = key;
    }

    /**
     * Get hash of encrypted file content.
     *
     * @return hash value
     */
    public byte[] getHash() {
        return hash;
    }

    /**
     * Set hash of encrypted file content.
     *
     * @param hash
     */
    public void setHash(byte[] hash) {
        this.hash = hash;
    }

    /**
     * Get relative path of locally stored file.
     *
     * @return relative file path
     */
    public String getRelativePath() {
        return relativePath;
    }

    /**
     * Set relative path of locally stored file
     *
     * @param relativePath relative file path
     */
    public void setRelativePath(String relativePath) {
        this.relativePath = relativePath;
    }

    /**
     * Get the owner of the file content.
     *
     * @return file owner
     */
    public User getOwner() {
        return owner;
    }

    /**
     * Set the owner of the file content
     *
     * @param owner file owner
     */
    public void setOwner(User owner) {
        this.owner = owner;
    }

    /**
     * Get the participant of the shared file.
     *
     * @return shared file participant
     */
    public User getParticipant() {
        return participant;
    }

    /**
     * Set shared file participant.
     *
     * @param participant user which is allowed to edit the shared file
     */
    public void setParticipant(User participant) {
        this.participant = participant;
    }

    /**
     * Get last file update time.
     *
     * @return last update time
     */
    public long getLastModified() {
        return lastModified;
    }

    /**
     * Set the last file update time.
     *
     * @param lastModified new last file update time
     */
    public void setLastModified(long lastModified) {
        this.lastModified = lastModified;
    }

    /**
     * Get flag if file has been deleted.
     *
     * @return true if file has been deleted else false
     */
    public boolean isDeleted() {
        return isDeleted;
    }

    /**
     * Set file deletion status.
     *
     * @param deleted true if file has been deleted else false
     */
    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    /**
     * Get encrypted file content.
     *
     * @return encrypted file content
     */
    public Content getContent() {
        return content;
    }

    /**
     * Set encrypted file content.
     *
     * @param content encrypted file content
     */
    public void setContent(Content content) {
        this.content = content;
    }

    /**
     * Get primary key (id) of meta data.
     *
     * @return meta data id
     */
    public long getId() {
        return id;
    }

    /**
     * Set primary key (id) of meta data.
     *
     * @param id unique meta data id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Get ID of referenced file content.
     *
     * @return ID of reference file content
     */
    public Long getContentId() {
        return contentId;
    }

    /**
     * Get ID of file owner.
     *
     * @return ID of file owner
     */
    public long getOwnerId() {
        return ownerId;
    }

    /**
     * Get ID of shared file participant.
     *
     * @return ID of file participant
     */
    public long getParticipantId() {
        return participantId;
    }

    /**
     * Get e-mail address of file owner.
     *
     * @return e-mail address of file owner
     */
    public String getOwnerEMail() {
        return ownerEMail;
    }

    /**
     * Get e-mail address of shared file participant.
     *
     * @return e-mail address of file participant
     */
    public String getParticipantEMail() {
        return participantEMail;
    }

    /**
     * After loading meta data from database initialize additional values like e-mail of
     * file owner and participant.
     */
    @PostLoad
    public void update() {

        this.ownerEMail = owner == null ? null : owner.getEmail();
        this.participantEMail = participant == null ? null : participant.getEmail();
    }
}