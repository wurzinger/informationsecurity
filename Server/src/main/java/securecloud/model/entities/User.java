package securecloud.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Database entity to store and identify a user.
 */
@Entity
@Table(name = "users")
public class User {

    /**
     * Primary key (id) of a user.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    private long id;

    /**
     * First name of a user.
     */
    @NotNull
    @NotEmpty
    private String firstName;

    /**
     * Last name of a user.
     */
    @NotNull
    @NotEmpty
    private String lastName;

    /**
     * E-mail address of a user.
     */
    @NotNull
    @NotEmpty
    @Email
    private String email;

    /**
     * Hashed user password.
     */
    @NotNull
    @NotEmpty
    private String password;

    /**
     * Password salt value.
     */
    @NotNull
    @NotEmpty
    private String salt;

    /**
     * Amount of false login attempts.
     */
    private Integer falseAttempts = 0;

    /**
     * Last login time.
     */
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastLogin = new Date();

    /**
     * Default JPA constructor.
     */
    public User() {

    }

    /**
     * Default JPA constructor.
     */
    public User(long id) {
        this.id = id;
    }

    /**
     * Get the e-mail address of user.
     *
     * @return e-mail address
     */
    public String getEmail() {
        return email;
    }

    /**
     * Set e-mail address of a user
     *
     * @param email e-mail address
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Get the hashed password of a user.
     *
     * @return hashed password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Set the hashed password of a user.
     *
     * @param password hashed password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Get the first name of a user.
     *
     * @return first name of user
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Set the first name of a user.
     *
     * @param firstName first name of a user
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Get the last name of a user.
     *
     * @return last name of user
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Set the last name of a user.
     *
     * @param lastName last name of a user
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Get the password salt value.
     *
     * @return salt value
     */
    public String getSalt() {
        return salt;
    }

    /**
     * Set the password salt value.
     *
     * @param salt salt value
     */
    public void setSalt(String salt) {
        this.salt = salt;
    }

    /**
     * Get the last login time.
     *
     * @return last login time
     */
    public Date getLastLogin() {
        return lastLogin;
    }

    /**
     * Set the last login time.
     *
     * @param lastLogin last login time
     */
    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    /**
     * Get the amount of false login attempts.
     *
     * @return false login attempts
     */
    public Integer getFalseAttempts() {
        return falseAttempts;
    }

    /**
     * Set the amount of false login attempts.
     *
     * @param falseAttempts false login attempts
     */
    public void setFalseAttempts(Integer falseAttempts) {
        this.falseAttempts = falseAttempts;
    }

    /**
     * Get the primary key (id) of a user.
     *
     * @return
     */
    public long getId() {

        return id;
    }
}