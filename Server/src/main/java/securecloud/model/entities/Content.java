package securecloud.model.entities;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Database entity to store a content of a file.
 * Content entity is stored in the contents table.
 */
@Entity
@Table(name = "contents")
public class Content {

    /**
     * Primary key (id) of content.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    /**
     * File content stored as byte[].
     */
    @NotNull
    @NotEmpty
    @Column(columnDefinition = "longblob")
    private byte[] content;

    public Content() {

    }

    /**
     * Returns the content of a file.
     *
     * @return stored content
     */
    public byte[] getContent() {
        return content;
    }

    /**
     * Sets the content of a file.
     *
     * @param content content to store
     */
    public void setContent(byte[] content) {
        this.content = content;
    }

    /**
     * Get content id.
     *
     * @return unique database id
     */
    public long getId() {
        return id;
    }

    /**
     * Set primary key (id) of content.
     *
     * @param id content id
     */
    public void setId(long id) {
        this.id = id;
    }
}
