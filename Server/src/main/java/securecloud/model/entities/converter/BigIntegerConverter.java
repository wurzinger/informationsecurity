package securecloud.model.entities.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.math.BigInteger;

/**
 * BigInteger to byte[] and vice versa converter.
 * Can be used to store a BigInteger value into a database as a byte[].
 */
@Converter
public class BigIntegerConverter implements AttributeConverter<BigInteger, byte[]> {

    /**
     * Convert BigInteger to byte[].
     *
     * @param value BigInteger value
     * @return corresponding byte[] of BigInteger value
     */
    @Override
    public byte[] convertToDatabaseColumn(BigInteger value) {
        return value.toByteArray();
    }

    /**
     * Convert byte[] to BigInteger.
     *
     * @param value byte[] value
     * @return corresponting BigInteger vo byte[] value
     */
    @Override
    public BigInteger convertToEntityAttribute(byte[] value) {
        return new BigInteger(value);
    }
}
