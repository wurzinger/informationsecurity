package securecloud.model.interfaces;

import java.math.BigInteger;

/**
 * RSA public key interface.
 */
public interface IRSAPublicKey {

    /**
     * Modulus of RSA public key.
     *
     * @return modulus
     */
    BigInteger getModulus();

    /**
     * Exponent of RSA public key.
     *
     * @return exponent
     */
    BigInteger getExponent();
}
