package securecloud.model.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Annotation to validate RSA public key.
 */
@Target({TYPE, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = RSAPublicKeyValidator.class)
@Documented
public @interface ValidRSAPublicKey {

    /**
     * Error message.
     *
     * @return
     */
    String message() default "Non valid RSA public key";

    /**
     * Default groups.
     *
     * @return
     */
    Class<?>[] groups() default {};

    /**
     * Default payload.
     *
     * @return
     */
    Class<? extends Payload>[] payload() default {};
}
