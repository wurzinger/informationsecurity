package securecloud.model.validators;

import securecloud.model.interfaces.IRSAPublicKey;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.security.KeyFactory;
import java.security.spec.RSAPublicKeySpec;

/**
 * RSA public key validator.
 */
public class RSAPublicKeyValidator implements ConstraintValidator<ValidRSAPublicKey, Object> {

    /**
     * Default initialization.
     *
     * @param constraintAnnotation
     */
    @Override
    public void initialize(ValidRSAPublicKey constraintAnnotation) {

    }

    /**
     * Validation of RSA public keys.
     *
     * @param obj IRSAPublicKey
     * @param context
     * @return true if asymmetric key is a valid RSA public key else false
     */
    @Override
    public boolean isValid(Object obj, ConstraintValidatorContext context) {

        try {
            final IRSAPublicKey key = (IRSAPublicKey) obj;
            final KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            final RSAPublicKeySpec publicKeySpec = new RSAPublicKeySpec(key.getModulus(), key.getExponent());
            keyFactory.generatePublic(publicKeySpec);
            return true;
        } catch (final Exception ex) {
            return false;
        }
    }
}