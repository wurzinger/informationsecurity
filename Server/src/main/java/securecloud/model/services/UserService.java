package securecloud.model.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import securecloud.model.entities.User;
import securecloud.model.exceptions.UserAlreadyExistsException;
import securecloud.model.exceptions.UserAuthenticationException;
import securecloud.model.exceptions.UserDoesNotExistException;
import securecloud.model.exceptions.UserIsLockedException;
import securecloud.model.repositories.UserRepository;

import java.security.MessageDigest;
import java.util.Calendar;
import java.util.Date;

/**
 * Service which provides basic user operations, like authentication and registration.
 */
@Service
public class UserService {

    /**
     * Maximum number of false login attempts.
     */
    private final int MAX_LOGIN = 3;

    @Autowired
    private UserRepository repository;

    /**
     * Register a new user.
     *
     * @param account account of the user to register.
     * @return stored account
     * @throws UserAlreadyExistsException if user (e-mail of user) is already in use
     */
    @Transactional
    public User register(User account) throws UserAlreadyExistsException {

        if (loadUser(account.getEmail()) != null) {
            throw new UserAlreadyExistsException("There is an account with that email address: " + account.getEmail());
        }

        repository.save(account);

        return account;
    }

    /**
     * Verify user credentials.
     * False attempt value is increased after an invalid login.
     * After a correct login, the false attempt amount is set to zero.
     *
     * @param email    user e-mail
     * @param password user password
     * @return user account if user credentials are correct
     * @throws UserIsLockedException       if false attempts are exceeded
     * @throws UserDoesNotExistException   if user is not registered
     * @throws UserAuthenticationException if user provided wrong credentials
     */
    @Transactional
    public User login(final String email, final String password)
            throws UserIsLockedException, UserDoesNotExistException, UserAuthenticationException {

        final User user = loadUser(email);

        if (user == null) {
            throw new UserDoesNotExistException();
        } else if (isLocked(user)) {
            throw new UserIsLockedException();
        } else {

            boolean correctPassword = false;

            // Java equals function is not safe (50ms for each correct position)
            if (MessageDigest.isEqual(password.getBytes(), user.getPassword().getBytes())) {
                correctPassword = true;
            }

            int falseAttempts = 0;
            if (!correctPassword) {
                falseAttempts = (user.getFalseAttempts() + 1) % MAX_LOGIN;
            }

            user.setFalseAttempts(falseAttempts);
            user.setLastLogin(new Date());
            repository.save(user);

            if (!correctPassword) {
                throw new UserAuthenticationException();
            }

            return user;
        }
    }

    /**
     * Load user by email address.
     *
     * @param email email address of user
     * @return user account if email of user is registered
     */
    @Transactional
    public User loadUser(final String email) {

        return repository.findByEmail(email);
    }

    /**
     * Load user by id.
     *
     * @param id database id of a user
     * @return user account if id exists
     */
    @Transactional
    public User loadUser(final long id) {

        return repository.findById(id);
    }

    /**
     * Load password salt value.
     *
     * @param email email address of user
     * @return password salt value
     * @throws UserDoesNotExistException if user is not registered
     */
    @Transactional
    public String getSalt(final String email) throws UserDoesNotExistException {

        final User user = repository.findByEmail(email);
        if (user == null) {
            throw new UserDoesNotExistException();
        }
        return user.getSalt();
    }

    /**
     * User will be locked for 15 minutes if amount incorrect logins
     * exceeds a predefined maximum number of false attempts.
     *
     * @param user user to verify
     * @return true if user can not login else false
     */
    private boolean isLocked(final User user) {

        boolean locked = true;

        if (user != null) {
            final Calendar c = Calendar.getInstance();
            c.setTime(user.getLastLogin());
            c.add(Calendar.MINUTE, 15);
            if (user.getFalseAttempts() < MAX_LOGIN || c.getTime().before(Calendar.getInstance().getTime())) {
                locked = false;
            }
        }

        return locked;
    }
}
