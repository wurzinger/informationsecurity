package securecloud.model.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import securecloud.model.entities.User;
import securecloud.model.repositories.UserRepository;

import java.util.List;

/**
 * Authentication security service.
 */
@Service
public class AuthenticationSecurityService implements UserDetailsService {

    @Autowired
    private UserRepository repository;

    /**
     * Load user details, like password and role, by a user name.
     *
     * @param userName user name
     * @return user details
     * @throws UsernameNotFoundException if user name does not exist
     */
    @Override
    public UserDetails loadUserByUsername(String userName)
            throws UsernameNotFoundException {

        User user = repository.findByEmail(userName);
        if (user == null) {
            return null;
        }

        List<GrantedAuthority> auth = AuthorityUtils
                .commaSeparatedStringToAuthorityList("ROLE_USER");
        if (userName.equals("admin")) {
            auth = AuthorityUtils
                    .commaSeparatedStringToAuthorityList("ROLE_ADMIN");
        }

        final String password = user.getPassword();
        return new org.springframework.security.core.userdetails.User(userName, password, auth);
    }
}