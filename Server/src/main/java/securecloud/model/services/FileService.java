package securecloud.model.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import securecloud.model.entities.Content;
import securecloud.model.entities.Data;
import securecloud.model.entities.MetaData;
import securecloud.model.entities.User;
import securecloud.model.exceptions.FileOutOfDateException;
import securecloud.model.exceptions.UnknownOwnerException;
import securecloud.model.exceptions.UserNotAuthorizedException;
import securecloud.model.repositories.ContentRepository;
import securecloud.model.repositories.MetaDataRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Service which provides file operations like storing, loading and updating
 * file contents and it's corresponding meta data.
 */
@Service
public class FileService {

    @Autowired
    private UserService userService;

    @Autowired
    private ContentRepository contentRepository;

    @Autowired
    private MetaDataRepository metaDataRepository;

    /**
     * Load content of a file.
     *
     * @param relativePath relative file path
     * @param owner        owner of the file
     * @param particpant   file participant (if file is not shared owner is participant too)
     * @return file content
     */
    @Transactional
    public Content loadContent(final String relativePath, final User owner, final User particpant) {

        MetaData meta = metaDataRepository.find(relativePath, owner, particpant);
        if (meta != null)
            return meta.getContent();
        return null;
    }

    /**
     * Load all meta data files for a specific participant.
     *
     * @param participant participant
     * @return participants meta data files
     */
    @Transactional
    public List<MetaData> loadMetaDataByParticipant(final User participant) {

        return metaDataRepository.findByParticipant(participant);
    }

    /**
     * Load a specific meta data file.
     *
     * @param relativePath relative file path
     * @param owner        owner of the file
     * @param participant  file participant (if file is not shared owner is participant too)
     * @return meta data file
     */
    @Transactional
    public MetaData loadMetaData(final String relativePath, final User owner, final User participant) {

        return metaDataRepository.find(relativePath, owner, participant);
    }

    /**
     * Load a specific meta data file.
     *
     * @param relativePath relative file path
     * @param participant  file participant
     * @return meta data file
     */
    @Transactional
    public MetaData loadMetaData(final String relativePath, final User participant) {

        return metaDataRepository.findByPathAndParticipant(relativePath, participant);
    }

    /**
     * Store meta data.
     *
     * @param data meta data to store
     * @return stored meta data
     */
    @Transactional
    public MetaData storeMetaData(final MetaData data) {

        metaDataRepository.save(data);
        return data;
    }

    /**
     * Store data file.
     * Content and meta data are stored separately.
     * After each storage a new last modified time is set for each file participant.
     *
     * @param user user who initiated the store action
     * @param file file to store
     * @return stored meta data file
     * @throws FileOutOfDateException if a newer version of the file exists on the server
     * @throws UnknownOwnerException  if owner of the file doesn't exist
     */
    @Transactional
    public MetaData storeFile(final User user, final Data file)
            throws FileOutOfDateException, UnknownOwnerException, UserNotAuthorizedException {

        Content content = file.getContent();
        MetaData metaData = file.getMetaData();

        final User owner = userService.loadUser(metaData.getOwnerEMail());
        if (owner == null) {
            throw new UnknownOwnerException();
        }

        metaData.setOwner(owner);
        metaData.setParticipant(user);

        final MetaData foundMetaData = metaDataRepository.findByPathAndParticipant(metaData.getRelativePath(), user);
        if (foundMetaData != null) {
            // check if a newer version exists
            // check if a shared file is not reinserted by a user, only owner can reset a shared file
            if (metaData.getLastModified() < foundMetaData.getLastModified()) {
                throw new FileOutOfDateException();
            }

            if((foundMetaData.isDeleted() && foundMetaData.getOwnerId() != user.getId() && owner.getId() != user.getId())) {
                throw new UserNotAuthorizedException(foundMetaData);
            }

            foundMetaData.setKey(metaData.getKey());
            foundMetaData.setIv(metaData.getIv());
            foundMetaData.setHash(metaData.getHash());

            metaData = foundMetaData;

            // if file has been deleted and stored again
            if (metaData.getContentId() != null) {
                content.setId(foundMetaData.getContentId());
            }
        }

        content = contentRepository.save(content);
        final long updateTime = new Date().getTime();
        if (metaData.isDeleted()) {
            metaData.setDeleted(false);
            metaData.setOwner(owner);
        }
        metaData.setContent(content);
        metaData.setLastModified(updateTime);
        metaDataRepository.save(metaData);
        metaDataRepository.updateLastModified(metaData.getRelativePath(), metaData.getOwner(), updateTime);

        return metaData;
    }

    /**
     * Delete a file.
     * Meta data file is only marked as deleted in the database.
     * If owner deletes a shared file
     * than the shared file will be deleted for all file participants.
     *
     * @param user     user who initiated the delete action
     * @param metaData meta data file to delete
     * @throws FileOutOfDateException if a newer version of the file exists on the server
     */
    @Transactional
    public long deleteFile(User user, MetaData metaData)
            throws FileOutOfDateException {

        final User owner = userService.loadUser(metaData.getOwnerEMail());
        final long updateTime = new Date().getTime();
        final MetaData foundMetaData = metaDataRepository.find(metaData.getRelativePath(), owner, user);
        if (foundMetaData != null) {
            if (metaData.getLastModified() < foundMetaData.getLastModified()) {
                throw new FileOutOfDateException();
            }

            final boolean isOwner = (user.getId() == foundMetaData.getOwner().getId()
                    && user.getId() == foundMetaData.getParticipant().getId());
            final List<MetaData> delete = new ArrayList<>();
            final Long contentId = foundMetaData.getContentId();

            if (isOwner)
                delete.addAll(metaDataRepository.findByPathAndOwner(metaData.getRelativePath(), foundMetaData.getOwner()));
            else delete.add(foundMetaData);

            for (MetaData toDelete : delete) {
                toDelete.setLastModified(updateTime);
                toDelete.setDeleted(true);
                toDelete.setHash(null);
                toDelete.setIv(null);
                toDelete.setKey(null);
                toDelete.setContent(null);
                metaDataRepository.save(toDelete);
            }

            if (isOwner) contentRepository.delete(contentId);
        }

        return updateTime;
    }

    /**
     * Unshare a file.
     * Meta data file is only marked as deleted in the database.
     *
     * @param user     user who initiated the unshare action
     * @param metaData meta data file to unshare
     * @throws FileOutOfDateException if a newer version of the file exists on the server
     */
    @Transactional
    public void unshareFile(User user, MetaData metaData)
            throws FileOutOfDateException, UserNotAuthorizedException {

        final User participant = userService.loadUser(metaData.getParticipantEMail());
        if (participant != null) {
            if (user.getId() == participant.getId()) {
                deleteFile(user, metaData);
            } else {
                final MetaData toDelete = metaDataRepository.find(metaData.getRelativePath(), user, participant);

                if (toDelete == null) {
                    throw new UserNotAuthorizedException();
                }

                /*
                if (metaData.getLastModified() < toDelete.getLastModified()) {
                    throw new FileOutOfDateException();
                }
                */

                toDelete.setLastModified(new Date().getTime());
                toDelete.setDeleted(true);
                toDelete.setHash(null);
                toDelete.setIv(null);
                toDelete.setKey(null);
                toDelete.setContent(null);
                metaDataRepository.save(toDelete);
            }
        }
    }
}
