package securecloud.model.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import securecloud.model.entities.PublicKey;
import securecloud.model.entities.User;
import securecloud.model.exceptions.PublicKeyNotFoundException;
import securecloud.model.repositories.PublicKeyRepository;

/**
 * Service which provides functions to store and fetch public keys of registered users.
 */
@Service
public class PublicKeyService {

    @Autowired
    private PublicKeyRepository repository;

    /**
     * Stores/updates a public key of a registered user.
     *
     * @param key public key to store
     */
    @Transactional
    public void store(final PublicKey key) {

        repository.deleteByUser(key.getUser());
        repository.save(key);
    }

    /**
     * Fetch public key.
     *
     * @param user owner of the public key
     * @return public key of user
     * @throws PublicKeyNotFoundException if owner doesn't have a public key
     */
    @Transactional
    public PublicKey load(final User user)
            throws PublicKeyNotFoundException {

        final PublicKey key = repository.findByUser(user);

        if (key == null) {
            throw new PublicKeyNotFoundException();
        }

        return key;
    }
}
