package securecloud.model.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import securecloud.model.entities.PublicKey;
import securecloud.model.entities.User;

import javax.transaction.Transactional;

/**
 * Repository for searching, creating, updating and deleting public key objects.
 */
@Transactional
@Repository
public interface PublicKeyRepository extends CrudRepository<PublicKey, Long> {

    /**
     * Find public key by user object.
     *
     * @param user user object
     * @return public key of the given user
     */
    PublicKey findByUser(User user);

    /**
     * Delete a public key by user object.
     *
     * @param user user object
     */
    void deleteByUser(User user);
}
