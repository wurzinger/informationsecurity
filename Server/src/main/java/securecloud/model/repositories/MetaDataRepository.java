package securecloud.model.repositories;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import securecloud.model.entities.MetaData;
import securecloud.model.entities.User;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Repository for searching, creating, updating and deleting meta data objects.
 */
@Transactional
@Repository
public interface MetaDataRepository extends CrudRepository<MetaData, Long> {

    /**
     * Load all meta data files for a specific participant.
     *
     * @param participant participant
     * @return participants meta data files
     */
    List<MetaData> findByParticipant(User participant);

    /**
     * Load a specific meta data file.
     *
     * @param relativePath relative file path
     * @param owner        owner of the file
     * @param participant  file participant (if file is not shared owner is participant too)
     * @return meta data file
     */
    @Query("select d from MetaData d where d.relativePath = ?1 and d.owner = ?2 and d.participant = ?3")
    MetaData find(String relativePath, User owner, User participant);

    /**
     * Load a specific meta data file.
     *
     * @param relativePath relative file path
     * @param participant  file participant
     * @return meta data file
     */
    @Query("select d from MetaData d where d.relativePath = ?1 and d.participant = ?2")
    MetaData findByPathAndParticipant(String relativePath, User participant);

    /**
     * Load a specific meta data file.
     *
     * @param relativePath relative file path
     * @param owner        file participant
     * @return meta data files
     */
    @Query("select d from MetaData d where d.relativePath = ?1 and d.owner = ?2")
    List<MetaData> findByPathAndOwner(String relativePath, User owner);

    /**
     * Update last modified time of a file.
     * If file is shared, last modified time values of all participants are updated.
     *
     * @param relativePath relative file path
     * @param owner        owner of the file
     * @param lastModified new last modified time value
     */
    @Modifying
    @Query("update MetaData d set last_modified = ?3 where d.relativePath = ?1 and d.owner = ?2")
    void updateLastModified(String relativePath, User owner, long lastModified);
}