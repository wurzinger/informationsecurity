package securecloud.model.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import securecloud.model.entities.Content;

import javax.transaction.Transactional;

/**
 * Repository for searching, creating, updating and deleting file content objects.
 */
@Transactional
@Repository
public interface ContentRepository extends CrudRepository<Content, Long> {

}
