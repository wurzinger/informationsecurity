package securecloud.model.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import securecloud.model.entities.User;

import javax.transaction.Transactional;

/**
 * Repository for searching, creating, updating and deleting user objects.
 */
@Transactional
@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    /**
     * Find user by a database id.
     * @param id user id
     * @return corresponding user object
     */
    User findById(long id);

    /**
     * Find user by an e-mail address.
     * @param email e-mail address of user
     * @return corresponding user object
     */
    User findByEmail(String email);
}
