package securecloud.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Default security initializer configuration.
 */
public class SecurityInitializerConfig extends AbstractSecurityWebApplicationInitializer {

}

